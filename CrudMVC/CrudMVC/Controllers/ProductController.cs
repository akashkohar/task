﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using CrudMVC.Models;

namespace CrudMVC.Controllers
{
    public class ProductController : Controller
    {string ConnectionString = @"data source = LAPR032; integrated security = false; initial catalog = registered; user id = sa; password = Akash@123";
        // GET: Product
        public ActionResult Index()
        {
            DataTable dtbProduct = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                
                sqlCon.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter("select * from details;",sqlCon);
                sqlAdapter.Fill(dtbProduct);
                
            }   
                return View(dtbProduct);
        }

        
        

        [HttpGet]
        public ActionResult Create()
        {
            return View(new ProductModel());
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(ProductModel Data)
        {
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                string query = "insert into details values(@userName,@phoneNumber,@email,@state)";
                SqlCommand sqlCmd = new SqlCommand(query,sqlCon);
                sqlCmd.Parameters.AddWithValue("@userName",Data.UserName);
                sqlCmd.Parameters.AddWithValue("@phoneNumber", Data.PhoneNumber);
                sqlCmd.Parameters.AddWithValue("@email", Data.Email);
                sqlCmd.Parameters.AddWithValue("@state", Data.AddState);
                sqlCmd.ExecuteNonQuery();
            }
            return RedirectToAction("index");
        }




      // PUT: Product/Edit/5
        [HttpPut]
        
        public ActionResult Edit1(string id)
        {
            ProductModel Data = new ProductModel();
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                string query = "update details set username=@userName,phonenumber=@phoneNumber,email=@email,Addstate=@state where username='"+id+"'";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@userName", Data.UserName);
                sqlCmd.Parameters.AddWithValue("@phoneNumber", Data.PhoneNumber);
                sqlCmd.Parameters.AddWithValue("@email", Data.Email);
                sqlCmd.Parameters.AddWithValue("@state", Data.AddState);
                sqlCmd.ExecuteNonQuery();
            }


            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(string name)
        {
            return Edit1(name);
        }
        // GET: Product/Delete/5
        public ActionResult Delete(string id)
        {
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                string query = "delete from details where username=@username";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@userName", id);
           
                sqlCmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

       
    }
}
