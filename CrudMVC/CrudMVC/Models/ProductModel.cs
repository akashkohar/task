﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CrudMVC.Models
{
    public class ProductModel
    {
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [DisplayName("Phone Number")]
        public long PhoneNumber { get; set; }
        public string Email { get; set; }
        [DisplayName("State")]
        public string AddState { get; set; }


    }
}