﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthenticationApp.Controllers
{
    public class AccountController : Controller
    {[AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login (string userName, string password)
        {
            if (!string.IsNullOrEmpty(userName) && string.IsNullOrEmpty(password))
            {
                return RedirectToAction("Login");
            }

            ClaimsIdentity identity = null;

            bool isAthenticated = false;

            if (userName == "Admin" && password == "123")
            {
                identity = new ClaimsIdentity(new[]{

                    new Claim(ClaimTypes.Name,userName),
                    new Claim(ClaimTypes.Role,"Admin")
                    },
                    CookieAuthenticationDefaults.AuthenticationScheme);
                isAthenticated = true;
            }
            if (userName == "Akash" && password == "123")
            {
                identity = new ClaimsIdentity(new[]{

                new Claim(ClaimTypes.Name,userName),
                new Claim(ClaimTypes.Role,"User")
                },
                CookieAuthenticationDefaults.AuthenticationScheme);
                isAthenticated = true;
            }
                if (isAthenticated)
                {
                     var principal = new ClaimsPrincipal(identity);

                     var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                     return RedirectToAction("Index", "Home");
                  
                }
            
            
            return View();
        }

        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }
    }
}