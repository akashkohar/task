﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FirstMVC.Models;
using System.Web.Mvc;
using System.Web;

namespace FirstMVC.Controllers
{
    public class MoviesController : ApiController
    {
        public ViewResult Random()
        {
            var movie = new Movie() { Name = "Bharat" };
            return View(movie);
        }
    }
}
