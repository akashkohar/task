﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EfCoreBasics.Models
{
    public class EmpData
    {
        [Key]
        public int Id { get; set; }
        public string Department { get; set; }
        public string City { get; set; }

    }
}
