﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EfCoreBasics.Models;

namespace EfCoreBasics.Controllers
{
    public class EmpDatasController : Controller
    {
        private readonly EmpContext _context;

        public EmpDatasController(EmpContext context)
        {
            _context = context;
        }

        // GET: EmpDatas
        public async Task<IActionResult> Index()
        {
            return View(await _context.EmpTable.ToListAsync());
        }

        // GET: EmpDatas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empData = await _context.EmpTable
                .FirstOrDefaultAsync(m => m.Id == id);
            if (empData == null)
            {
                return NotFound();
            }

            return View(empData);
        }

        // GET: EmpDatas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EmpDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Department,City")] EmpData empData)
        {
            if (ModelState.IsValid)
            {
                _context.Add(empData);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(empData);
        }

        // GET: EmpDatas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empData = await _context.EmpTable.FindAsync(id);
            if (empData == null)
            {
                return NotFound();
            }
            return View(empData);
        }

        // POST: EmpDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Department,City")] EmpData empData)
        {
            if (id != empData.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(empData);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmpDataExists(empData.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(empData);
        }

        // GET: EmpDatas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empData = await _context.EmpTable
                .FirstOrDefaultAsync(m => m.Id == id);
            if (empData == null)
            {
                return NotFound();
            }

            return View(empData);
        }

        // POST: EmpDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var empData = await _context.EmpTable.FindAsync(id);
            _context.EmpTable.Remove(empData);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmpDataExists(int id)
        {
            return _context.EmpTable.Any(e => e.Id == id);
        }
    }
}
