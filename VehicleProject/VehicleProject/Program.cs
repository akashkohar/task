﻿using System;

namespace Automobile
{
    public class Vehicle
    {
        private string _vehicleName;
        private static string _vehicleColour;
        public readonly int maxSpeed;

        public int initialSpeed;


        public static string VehicleColour
        {
            get
            {

                return _vehicleColour;

            }
            set
            {
                _vehicleColour = value;
            }

        }
        public Vehicle(string defaultName)
        {
            _vehicleName = defaultName;
            initialSpeed = 0;
            maxSpeed = 60;
        }
        private static int _noOfWheels;
        public static int NoOfwheels
        {
            get
            {
                return _noOfWheels;
            }

            set
            {
                _noOfWheels = value;
            }
        }

       public enum NumberOfWheels
        {
            car = 4,
            bike=2
        }
 



        public void Stop()

        {

            Console.WriteLine("vehicle is stopped");
        }

        public void SpeedUp(int addSpeed, int maxSpeed)
        {
            if (initialSpeed >= 60)
            {
                Console.WriteLine("you are going over max speed");

            }
            else
            {
                initialSpeed += addSpeed;

            }
        }

        public void Functioning()
        {


            int start = 1;
            char ans;
            string localName;


            Console.WriteLine("Enter the Vehicle Colour ");
            Vehicle.VehicleColour = Console.ReadLine();




        }
    }
    public class Bike : Vehicle
    {
        private string _BikeName;
        private static string _BikeColour;
        public readonly int maxSpeed;

        public int initialSpeed;
        public void Start()
        {

            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + (int)NumberOfWheels.bike);
            Console.WriteLine("Vehicle colour= " + VehicleColour);
            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your speed is=" + initialSpeed);



        }


        public Bike(string defaultName) : base(defaultName)
        {
            _BikeName = defaultName;
            maxSpeed = 60;
            
        }

        public void CalculateToll()
        {

            Console.WriteLine("toll cost for bike is rs 20 ");


        }


    }


    public class Car : Vehicle
    {
        private string _carName;
        private static string _carColour;
        public readonly int maxSpeed;

        public int initialSpeed;

        public Car(string defaultName) : base(defaultName)
        {
            _carName = defaultName;
            
            maxSpeed = 60;
        }

        public void Start()
        {

            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + (int)NumberOfWheels.car);
            Console.WriteLine("Vehicle colour= " + VehicleColour);
            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your speed is=" + initialSpeed);



        }
        public void CalculateToll()
        {

            Console.WriteLine("toll cost for car is rs  40");


        }
    }

    class Control
    {
        public static void Main(string[] args)
        {
            int key, start = 1;
            string localName;
            Char ans;
            Console.WriteLine("Enter the name vehicle type (1)Bike  (2)Car");
            key = Convert.ToInt32(Console.ReadLine());

            if (key == 1)
            {
                Console.WriteLine("Enter the Vehicle Name ");
                localName = Console.ReadLine();
                Bike input = new Bike(localName);

                input.Functioning();
                input.CalculateToll();
                do
                {

                    if (start == 1)
                    {
                        input.Start();
                        Console.WriteLine("do you want to speed up(y/n)");
                        ans = Convert.ToChar(Console.ReadLine());
                        if (ans == 'y' || ans == 'Y')
                        {

                            input.SpeedUp(30, input.maxSpeed);
                        }
                    }
                    else
                        input.Stop();
                    Console.WriteLine("Do you want to (1)keep going or (2)Stop your ride(1/2)");
                    start = Convert.ToInt32(Console.ReadLine());


                } while (start == 1);

                input.Stop();
            }
            if (key == 2)
            {
                Console.WriteLine("Enter the Vehicle Name ");
                localName = Console.ReadLine();
                Car input = new Car(localName);
                input.Functioning();
                input.CalculateToll();
                do
                {

                    if (start == 1)
                    {
                        input.Start();
                        Console.WriteLine("do you want to speed up(y/n)");
                        ans = Convert.ToChar(Console.ReadLine());
                        if (ans == 'y' || ans == 'Y')
                        {

                            input.SpeedUp(30, input.maxSpeed);
                        }
                    }
                    else
                        input.Stop();
                    Console.WriteLine("Do you want to (1)keep going or (2)Stop your ride(1/2)");
                    start = Convert.ToInt32(Console.ReadLine());


                } while (start == 1);

                input.Stop();
            }

        }

    }

}
