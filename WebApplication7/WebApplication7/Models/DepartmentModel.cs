﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication7.Models
{
    public class DepartmentModel
    {
        public int Id { get; set; }
        public int Count { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string DepartmentName { get; set; }
    }
}
