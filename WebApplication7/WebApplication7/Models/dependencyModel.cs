﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication7.Models
{
    public class DependencyModel
    {

        private readonly DataContext _context;

        public object Employees { get; internal set; }


        public DependencyModel(DataContext context)
        {
            _context = context;
        }


        public void Add(EmployeeModel emp)
        {
            _context.Add(emp);

        }
        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
        }
    }
}
