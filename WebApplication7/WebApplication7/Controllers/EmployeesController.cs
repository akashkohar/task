﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using DataLayer.Entities;
using WebApplication7.Models;

namespace WebApplication7.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly DataContext _context;

        public EmployeesController(DataContext context)
        {
            _context = context;
        }

        // GET: Employees
        public async Task<IActionResult> Index(int? id)

        {
            if (id != null)
            {
                if (ModelState.IsValid)
                {
                    var dataContext = _context.Employees.Where(e => id == e.DepartmentId).Select(e => new EmployeeModel
                    {
                        Id = e.Id,
                        UserName = e.UserName,
                        DOB = e.DOB,
                        Phone = e.Phone,
                        Emial = e.Emial,
                        Address = e.Address,
                        State = e.State,
                        City = e.City,
                        Zip = e.Zip,
                        DepartmentName = e.Departments.DepartmentName,
                        DepartmentId = e.DepartmentId
                    });

                    return View(await dataContext.ToListAsync());


                }
                else { return RedirectToRoute("Index"); }
            }
            else if (ModelState.IsValid)
            {



                var dataContext = _context.Employees.Include(e => e.Departments).Select(e => new EmployeeModel
                {
                    Id = e.Id,
                    UserName = e.UserName,
                    DOB = e.DOB,
                    Phone = e.Phone,
                    Emial = e.Emial,
                    Address = e.Address,
                    State = e.State,
                    City = e.City,
                    Zip = e.Zip,
                    DepartmentName = e.Departments.DepartmentName,
                    DepartmentId = e.DepartmentId
                });

                return View(await dataContext.ToListAsync());
            }

            else { return RedirectToRoute("Index"); }

        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var employee = await _context.Employees
                .Include(e => e.Departments)
                .FirstOrDefaultAsync(m => m.Id == id);

            EmployeeModel emp = new EmployeeModel();
            emp.Id = employee.Id;
            emp.UserName = employee.UserName;
            emp.DOB = employee.DOB;
            emp.City = employee.City;
            emp.Address = employee.Address;

            emp.Emial = employee.Emial;
            emp.State = employee.State;
            emp.Phone = employee.Phone;
            emp.DepartmentName = employee.Departments.DepartmentName;
            emp.Zip = employee.Zip;
            emp.DepartmentId = employee.DepartmentId;

            if (employee == null)
            {
                return NotFound();
            }
            return View(emp);
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            List<Department> departments = _context.Departments.ToList();
            ViewBag.DepartmentName = new SelectList(departments, "DepartmentName", "DepartmentName");
            ViewData["State"] = new SelectList(_context.States, "State", "State");

            ViewData["Id"] = new SelectList(_context.States, "Id", "Id");
            
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Id");
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,DOB,Phone,Emial,Address,State,City,Zip,DepartmentId")] Employee employee)
        { 
          
            
            if (ModelState.IsValid)
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
          //  ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Id", employee.DepartmentId);
            return View(employee);
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            
            List<Department> departments = _context.Departments.ToList();
            ViewBag.DepartmentName = new SelectList(departments, "DepartmentName", "DepartmentName");
            ViewData["State"] = new SelectList(_context.States, "State", "State");
            

            ViewData["Id"] = new SelectList(_context.States, "Id", "Id");

            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Id");

            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            EmployeeModel emp = new EmployeeModel();
            emp.Id = employee.Id;
            emp.UserName = employee.UserName;
            emp.DOB = employee.DOB;
            emp.City = employee.City;
            emp.Address = employee.Address;
            ViewData["City"] = new SelectList(_context.Cities.Where(x => x.States.State == employee.State), "City", "City");
            emp.Emial = employee.Emial;
            emp.State = employee.State;
            emp.Phone = employee.Phone;
            emp.Zip = employee.Zip;
            emp.DepartmentId = employee.DepartmentId;
            emp.DepartmentName = employee.Departments.DepartmentName;


            return View(emp);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id,Employee employee)
        {
            if (id != employee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "Id", "Id", employee.DepartmentId);
            return View(employee);
        }

        // GET: Employees/Delete/5

        public async Task<IActionResult> Delete(int? EmployeeId)
        {
            if (EmployeeId == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .Include(e => e.Departments)
                .FirstOrDefaultAsync(m => m.Id == EmployeeId);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }
       
        public IActionResult FindCity(string id)
        {
            //var sId = _context.States.Where(z => z.State == name).Select(x => x.Id);
            //int id = Convert.ToInt32(sId);
            var city = _context.Cities.Where(z=>id==z.States.State).Select(x => new 
            {
                
                 Id = x.Id,
                City = x.City

            });
           
            return new JsonResult(city);

        }
        public IActionResult FindId(string id)
        {
            var city = _context.Departments.Where(z => id == z.DepartmentName).Select(x => new
            {

                name = x.Id,
                
            });

            return new JsonResult(city);

        }
    }
}
