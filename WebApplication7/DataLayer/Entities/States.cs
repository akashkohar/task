﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Entities
{
    public class States
    {
        public int Id { get; set; }
        public string State { get; set; }
        public ICollection<Cities> Cities { get; set; }
    }
}
