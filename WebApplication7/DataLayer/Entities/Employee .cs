﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Entities
{
   public class Employee
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        
        public DateTime DOB { get; set; }
        public long Phone { get; set; }
        public string Emial { get; set; }
        public string  Address  { get; set; }
        public string  State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        
        public int DepartmentId { get; set; }

        public Department Departments { get; set; }
    }
}
