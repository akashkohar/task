﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Entities
{
    public class Department
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        
        public ICollection<Employee> CurrentEmp { get; set; }

    }
}
