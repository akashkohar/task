﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Entities
{
   public class Cities
    {
        public int Id { get; set; }
        public int StateId { get; set; }
        public string City { get; set; }
        public States States { get; set; }
    }

}
