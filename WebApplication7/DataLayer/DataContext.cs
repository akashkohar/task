﻿using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace DataLayer
{
    public class DataContext : DbContext
    {
        private string _connectionString;
        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }
       

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<States> States { get; set; }
        public virtual DbSet<Cities> Cities { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);

            }
            }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Department>(data =>
            {
                data.ToTable("Dpartments");

                data.HasKey(prime => prime.Id);
                
               
            });
            modelBuilder.Entity<Employee>(emp => emp.HasKey(prime => prime.Id));

            modelBuilder.Entity<Employee>()
                .HasOne<Department>(dept => dept.Departments)
                .WithMany(relation => relation.CurrentEmp)
                .HasForeignKey(key => key.DepartmentId);

            modelBuilder.Entity<States>(s =>
            {
                s.ToTable("States");
                s.HasKey(id => id.Id);
                            });
            modelBuilder.Entity<Cities>()
                        .HasOne<States>(s => s.States)
                        .WithMany(c => c.Cities)
                        .HasForeignKey(fk => fk.StateId);
        }




    }

        
    }

