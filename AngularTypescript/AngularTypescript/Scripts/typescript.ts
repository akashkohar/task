﻿let answer: boolean = true;
let digit: number = 10; // number can also strore floating , decimal and binary values
let firstName: string = "Akash"; // we can also use single qoutes
let lastName: string =  `Kohar`  ;
let fullName: string = `my full name is ${firstName} ${lastName}`; // here we have concatinated both the strings 
//array
let list: number[] = [1, 2, 3]; // we can also use <> for defining the type of the array
//tuple
// Declare a tuple type
let tuple: [string, number];
// Initialize it
tuple = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"];  Error

//enum
//an enum is a way of giving more friendly names to sets of numeric values
enum Color { Red = 1, Green, Blue }
let c: Color = Color.Green;

//any
let undefine: any = 4;
undefine = "maybe a string instead";
undefine = false; //  boolean
//void
//this type can only assign null
function warnUser(): void {              // this function return null
    console.log("This is my warning message");
}

let unusable: void = undefined;    // this variable can only hold null value
unusable = null;

//null and undefined
let u: undefined = undefined;
let n: null = null;
//never
// Function returning never must have unreachable end point
function error(message: string): never {
    throw new Error(message);
}

//var
function f() {
    var message = "Hello Akash";

    return message;
}
//var and let scopes
function fun(input: boolean) {
    let a = 100;
   
    if (input) {
        // 'a' exist here
        let b = a + 1;
       
        return b;
      
    }

    // 'b' doesn't exist here
    //return b;
}

//const
const rollNumber = 9;
const kitty = {
    name: "Akash",
    num: rollNumber,
}

// Error
//kitty = {
//    name: "Rohit",
//    numLives: rollNumber
//};

// all "okay"
kitty.name = "Rohit";
kitty.name = "Raju";
kitty.name = "Ramesh";
kitty.num--;



//object destructing


let ob = {
    a: "foo",
    b: 12,
    c: "bar"
};
let { a, b } = ob;
({ a, b } = { a: "baz", b: 101 });

//Classes
class Greeter {
    greeting: string;  //by default public
    constructor(message: string) {  
        this.greeting = message;
    }
    greet() {
        return "Hello, " + this.greeting;
    }
}

let greeter = new Greeter("world");  // creating object of the class
greeter.greet();

//inheritance
// we can use common object-oriented patterns
class Car {
    move(distanceInMeters: number = 0) {
        console.log(`Car moved ${distanceInMeters}m.`);
    }
}

class Horn extends Car {
    bark() {
        console.log('honk!!!');
    }
}

const horn = new Horn();
horn.bark();
horn.move(10);
horn.bark();

//private members
//TypeScript also has private, it cannot be accessed from outside of its containing class.For example:

class Cars {
    private name: string;
    constructor(theName: string) { this.name = theName; }
}

//new Cars("Maruti").name; // Error: 'name' is private;

//protected
//the protected modifier acts much like the private modifier with the exception 
//that members declaredprotected can also be accessed within deriving classes
class Animal {
    protected name: string;
    constructor(theName: string) { this.name = theName; }
}

//new Animal("Cat").name;

//static properties
class Grid {
    static origin = { x: 0, y: 0 };
    calculateDistanceFromOrigin(point: { x: number; y: number; }) {
        let xDist = (point.x - Grid.origin.x);
        let yDist = (point.y - Grid.origin.y);
        return Math.sqrt(xDist * xDist + yDist * yDist) / this.scale;
    }
    constructor(public scale: number) { }
}

let grid1 = new Grid(1.0);  // 1x scale
let grid2 = new Grid(5.0);  // 5x scale

console.log(grid1.calculateDistanceFromOrigin({ x: 10, y: 10 }));
console.log(grid2.calculateDistanceFromOrigin({ x: 10, y: 10 }));

//abstract class
abstract class Truck {
    abstract makeSound(): void;
    move(): void {
        console.log("Moving");
    }
}

//using class as an interface
class Point {
    x: number;
    y: number;
}

interface Point3d extends Point {
    z: number;
}

let point3d: Point3d = { x: 1, y: 2, z: 3 };


//Functions #
// Named function
function add(x, y) {
    return x + y;
}

// Anonymous function
let myAdd = function (x, y) { return x + y; };

//rest parameter
function buildName(firstName: string, ...restOfName: string[]) {
    return firstName + " " + restOfName.join(" ");
}

// employeeName will be "Akash Singh Kohar"
let employeeName = buildName("Akash", "Singh", "KOhar");

//function overloading
let suits = ["hearts", "spades", "clubs", "diamonds"];

function pickCard(x: { suit: string; card: number; }[]): number;
function pickCard(x: number): { suit: string; card: number; };
function pickCard(x): any {
    // Check to see if we're working with an object/array
    // if so, they gave us the deck and we'll pick the card
    if (typeof x == "object") {
        let pickedCard = Math.floor(Math.random() * x.length);
        return pickedCard;
    }
    // Otherwise just let them pick the card
    else if (typeof x == "number") {
        let pickedSuit = Math.floor(x / 13);
        return { suit: suits[pickedSuit], card: x % 13 };
    }
}

//interfaces
interface LabeledValue {
    label: string;
}
function printLabel(labeledObj: LabeledValue) {
    console.log(labeledObj.label);
}
let myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);

//Readonly properties #


interface Points {
    readonly num1: number;
    readonly num2: number;
}
let p1: Points = { num1: 10, num2: 20 };
//p1.x = 5; // error!

//implimenting interface with class
interface ClockInterface {
    currentTime: Date;
    setTime(d: Date): void;
}
class Clock implements ClockInterface {
    currentTime: Date = new Date();
    setTime(d: Date) {
        this.currentTime = d;
    }
    constructor(h: number, m: number) { }
}

//Generics
//working with generic type 
function loggingIdentity<T>(arg: T): T {
    //console.log(arg.length);  // Error= T doesn't have .length
    return arg;
}

//gneric classes
class GenericNumber<T> {
    zeroValue: T;
    add: (x: T, y: T) => T;
}
let myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };

//Generic Constraints
interface Lengthwise {
    length: number;
}

function loggingIdentities<T extends Lengthwise>(arg: T): T {
    console.log(arg.length);  // Now we know it has a .length property, so no more error
    return arg;
}

//Enums
//enums can make it easier to document intent, or create a set of distinct cases. 
//TypeScript provides both numeric and string - based enums
//Numeric enums
enum Direction {
    Up = 1,
    Down,
    Left,
    Right,
}
//String enums
enum Directions {
    Up = "UP",
    Down = "DOWN",
    Left = "LEFT",
    Right = "RIGHT",
}

// Heterogeneous enums 
// Technically enums can be mixed with string and numeric members

    enum BooleanLikeHeterogeneousEnum {
        No = 0,
        Yes = "YES",
    }

//iterators

let list1 = [4, 5, 6];
// for in
for (let i in list1) {
    console.log(i); // "0", "1", "2",
}
// for of
for (let i of list1) {
    console.log(i); // "4", "5", "6"
}
//for
for (var _i = 0; _i < list1.length; _i++) {
    var num = list1[_i];
    console.log(num);
}