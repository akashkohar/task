var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var answer = true;
var digit = 10; // number can also strore floating , decimal and binary values
var firstName = "Akash"; // we can also use single qoutes
var lastName = "Kohar";
var fullName = "my full name is " + firstName + " " + lastName; // here we have concatinated both the strings 
//array
var list = [1, 2, 3]; // we can also use <> for defining the type of the array
//tuple
// Declare a tuple type
var tuple;
// Initialize it
tuple = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"];  Error
//enum
//an enum is a way of giving more friendly names to sets of numeric values
var Color;
(function (Color) {
    Color[Color["Red"] = 1] = "Red";
    Color[Color["Green"] = 2] = "Green";
    Color[Color["Blue"] = 3] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
//any
var undefine = 4;
undefine = "maybe a string instead";
undefine = false; //  boolean
//void
//this type can only assign null
function warnUser() {
    console.log("This is my warning message");
}
var unusable = undefined; // this variable can only hold null value
unusable = null;
//null and undefined
var u = undefined;
var n = null;
//never
// Function returning never must have unreachable end point
function error(message) {
    throw new Error(message);
}
//var
function f() {
    var message = "Hello Akash";
    return message;
}
//var and let scopes
function fun(input) {
    var a = 100;
    if (input) {
        // 'a' exist here
        var b_1 = a + 1;
        return b_1;
    }
    // 'b' doesn't exist here
    //return b;
}
//const
var rollNumber = 9;
var kitty = {
    name: "Akash",
    num: rollNumber,
};
// Error
//kitty = {
//    name: "Rohit",
//    numLives: rollNumber
//};
// all "okay"
kitty.name = "Rohit";
kitty.name = "Raju";
kitty.name = "Ramesh";
kitty.num--;
//object destructing
var ob = {
    a: "foo",
    b: 12,
    c: "bar"
};
var a = ob.a, b = ob.b;
(_a = { a: "baz", b: 101 }, a = _a.a, b = _a.b);
//Classes
var Greeter = /** @class */ (function () {
    function Greeter(message) {
        this.greeting = message;
    }
    Greeter.prototype.greet = function () {
        return "Hello, " + this.greeting;
    };
    return Greeter;
}());
var greeter = new Greeter("world"); // creating object of the class
greeter.greet();
//inheritance
// we can use common object-oriented patterns
var Car = /** @class */ (function () {
    function Car() {
    }
    Car.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 0; }
        console.log("Car moved " + distanceInMeters + "m.");
    };
    return Car;
}());
var Horn = /** @class */ (function (_super) {
    __extends(Horn, _super);
    function Horn() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Horn.prototype.bark = function () {
        console.log('honk!!!');
    };
    return Horn;
}(Car));
var horn = new Horn();
horn.bark();
horn.move(10);
horn.bark();
//private members
//TypeScript also has private, it cannot be accessed from outside of its containing class.For example:
var Cars = /** @class */ (function () {
    function Cars(theName) {
        this.name = theName;
    }
    return Cars;
}());
//new Cars("Maruti").name; // Error: 'name' is private;
//protected
//the protected modifier acts much like the private modifier with the exception 
//that members declaredprotected can also be accessed within deriving classes
var Animal = /** @class */ (function () {
    function Animal(theName) {
        this.name = theName;
    }
    return Animal;
}());
//new Animal("Cat").name;
//static properties
var Grid = /** @class */ (function () {
    function Grid(scale) {
        this.scale = scale;
    }
    Grid.prototype.calculateDistanceFromOrigin = function (point) {
        var xDist = (point.x - Grid.origin.x);
        var yDist = (point.y - Grid.origin.y);
        return Math.sqrt(xDist * xDist + yDist * yDist) / this.scale;
    };
    Grid.origin = { x: 0, y: 0 };
    return Grid;
}());
var grid1 = new Grid(1.0); // 1x scale
var grid2 = new Grid(5.0); // 5x scale
console.log(grid1.calculateDistanceFromOrigin({ x: 10, y: 10 }));
console.log(grid2.calculateDistanceFromOrigin({ x: 10, y: 10 }));
//abstract class
var Truck = /** @class */ (function () {
    function Truck() {
    }
    Truck.prototype.move = function () {
        console.log("Moving");
    };
    return Truck;
}());
//using class as an interface
var Point = /** @class */ (function () {
    function Point() {
    }
    return Point;
}());
var point3d = { x: 1, y: 2, z: 3 };
//Functions #
// Named function
function add(x, y) {
    return x + y;
}
// Anonymous function
var myAdd = function (x, y) { return x + y; };
//rest parameter
function buildName(firstName) {
    var restOfName = [];
    for (var _a = 1; _a < arguments.length; _a++) {
        restOfName[_a - 1] = arguments[_a];
    }
    return firstName + " " + restOfName.join(" ");
}
// employeeName will be "Akash Singh Kohar"
var employeeName = buildName("Akash", "Singh", "KOhar");
//function overloading
var suits = ["hearts", "spades", "clubs", "diamonds"];
function pickCard(x) {
    // Check to see if we're working with an object/array
    // if so, they gave us the deck and we'll pick the card
    if (typeof x == "object") {
        var pickedCard = Math.floor(Math.random() * x.length);
        return pickedCard;
    }
    // Otherwise just let them pick the card
    else if (typeof x == "number") {
        var pickedSuit = Math.floor(x / 13);
        return { suit: suits[pickedSuit], card: x % 13 };
    }
}
function printLabel(labeledObj) {
    console.log(labeledObj.label);
}
var myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);
var p1 = { num1: 10, num2: 20 };
var Clock = /** @class */ (function () {
    function Clock(h, m) {
        this.currentTime = new Date();
    }
    Clock.prototype.setTime = function (d) {
        this.currentTime = d;
    };
    return Clock;
}());
//Generics
//working with generic type 
function loggingIdentity(arg) {
    //console.log(arg.length);  // Error= T doesn't have .length
    return arg;
}
//gneric classes
var GenericNumber = /** @class */ (function () {
    function GenericNumber() {
    }
    return GenericNumber;
}());
var myGenericNumber = new GenericNumber();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };
function loggingIdentities(arg) {
    console.log(arg.length); // Now we know it has a .length property, so no more error
    return arg;
}
//Enums
//enums can make it easier to document intent, or create a set of distinct cases. 
//TypeScript provides both numeric and string - based enums
//Numeric enums
var Direction;
(function (Direction) {
    Direction[Direction["Up"] = 1] = "Up";
    Direction[Direction["Down"] = 2] = "Down";
    Direction[Direction["Left"] = 3] = "Left";
    Direction[Direction["Right"] = 4] = "Right";
})(Direction || (Direction = {}));
//String enums
var Directions;
(function (Directions) {
    Directions["Up"] = "UP";
    Directions["Down"] = "DOWN";
    Directions["Left"] = "LEFT";
    Directions["Right"] = "RIGHT";
})(Directions || (Directions = {}));
// Heterogeneous enums 
// Technically enums can be mixed with string and numeric members
var BooleanLikeHeterogeneousEnum;
(function (BooleanLikeHeterogeneousEnum) {
    BooleanLikeHeterogeneousEnum[BooleanLikeHeterogeneousEnum["No"] = 0] = "No";
    BooleanLikeHeterogeneousEnum["Yes"] = "YES";
})(BooleanLikeHeterogeneousEnum || (BooleanLikeHeterogeneousEnum = {}));
//iterators
var list1 = [4, 5, 6];
// for in
for (var i in list1) {
    console.log(i); // "0", "1", "2",
}
// for of
for (var _b = 0, list1_1 = list1; _b < list1_1.length; _b++) {
    var i = list1_1[_b];
    console.log(i); // "4", "5", "6"
}
//for
for (var _i = 0; _i < list1.length; _i++) {
    var num = list1[_i];
    console.log(num);
}
//# sourceMappingURL=typescript.js.map