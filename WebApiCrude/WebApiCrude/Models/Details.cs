﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiCrude.Models
{
    public class Details
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

    }
}