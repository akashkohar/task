﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace WebApi.Controllers
{
   
    public class ActionFilter : ActionFilterAttribute
    {

        public string Id { get; set; }

        public ActionFilter(string actionName)
        {
            Id = actionName;
        }


        public override void OnActionExecuting(HttpActionContext actionContext)
        {
           
            //var action = actionContext.ActionDescriptor.ActionName;

            Console.WriteLine("This is before Execution of Action ");
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            Console.WriteLine("This is After Execution of Action ");
        }



    }

    public class actionFilter2 : ActionFilterAttribute
    {
        public string Id { get; set; }

        public actionFilter2(string action)
        {
            Id = action;
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var action = actionContext.ActionDescriptor.ActionName;

            Console.WriteLine($"{Id} before = {action}");
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;

            Console.WriteLine($"{Id} before = {action}");
        }
    }
    public class FilterController : ApiController
    {
        [ActionFilter("MyFilter 1")]
        [Route("api/My/firstFilter")]
        public string GetFirstActionFilter()
        {
            Console.WriteLine("Action is Executing ");
            return "inside Get First Action Filter method";
        }
        [actionFilter2("MyFilter 2")]
        [Route("api/My/secondFilter")]
        public string GetSecondActionFilter()
        {
            Console.WriteLine("welcome to Seccond filter");
            return "inside Get second Action Filter method";
        }
    }
}
