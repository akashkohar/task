﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiCrude.Models;

namespace WebApiCrude.controllers
{
    public class DocumentController : ApiController

    {   
        SqlConnection myConnection = new SqlConnection();
        SqlCommand sqlCmd = new SqlCommand();
        public SqlConnection Connect()
        {
            try
            {
              myConnection.ConnectionString = @"data source = LAPR032; integrated security = false; initial catalog = WebApi; user id = sa; password = Akash@123";
            }
            catch (Exception error) { Console.WriteLine("Error"+error); }
             
            return myConnection;
                
        }
        [HttpGet]
        [ActionName("GetAllPersonDetails")]
        public DataSet Get()
        {
            DataSet data = new DataSet();
           
            myConnection=Connect();
        

            try
            {
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "select * from employee";
                sqlCmd.Connection = myConnection;
                myConnection.Open();
               
                SqlDataAdapter dataEmployee = new SqlDataAdapter(sqlCmd);
                dataEmployee.Fill(data);

            }

            catch (Exception error)
            {
                Console.WriteLine(error);
            }

            finally
            {
                myConnection.Close();

            }

            return data;

        }
        //[HttpGet]
        //[ActionName("GetEmployeeByID")]
        //public Details Get(int id)
        //{
            
        //    SqlDataReader reader = null;
          
        //    Details emp = null;

        //    try
        //    {
        //        //return listEmp.First(e => e.ID == id);  

        //        myConnection = Connect();

        //        sqlCmd.CommandType = CommandType.Text;
        //        sqlCmd.CommandText = "Select * from document where id=" + id + "";
        //        sqlCmd.Connection = myConnection;
        //        myConnection.Open();
        //        reader = sqlCmd.ExecuteReader();


        //        if (reader.Read())
        //        {
        //            emp = new Details();
        //            emp.Name = reader.GetValue(0).ToString();
        //            emp.Department = reader.GetValue(1).ToString();
        //            emp.Id = Convert.ToInt32(reader.GetValue(2));

        //        }

        //    }
        //    catch (Exception error)
        //    {
        //        Console.WriteLine("Error"+error);
        //    }
        //    finally
        //    {
        //        myConnection.Close();
        //    }

        //    return emp;


        //}
        //[HttpPut]
        
        //public void Update(int id,string username)
        //{
        //    myConnection = Connect();

        //   sqlCmd.CommandType = CommandType.Text;
        //    sqlCmd.CommandText = "update document set username='"+username+"' where id="+id+"";
        //    sqlCmd.Connection = myConnection;
        //    myConnection.Open();
        //    int rowUpdated = sqlCmd.ExecuteNonQuery();
        //    myConnection.Close();
        //}
        [HttpPost]
        [ActionName("NewEmployee")]
        public void AddEmployee([FromBody]Details employee)
        {
           
            
                myConnection = Connect();
                myConnection.Open();
                sqlCmd.CommandType = CommandType.Text;
                
                sqlCmd.CommandText = "insert into Employee(UserName, Birthday, Phone, Email, Department, UserAddress, UserState, City, Zip) values(@userName,@birthday,@phone,@email,@department,@address,@state,@city,@zip)";
                sqlCmd.Connection = myConnection;

                sqlCmd.Parameters.AddWithValue("@userName", employee.Name);
                sqlCmd.Parameters.AddWithValue("@birthday", employee.Date);
                sqlCmd.Parameters.AddWithValue("@phone", employee.Phone);
                sqlCmd.Parameters.AddWithValue("@email", employee.Email);
                sqlCmd.Parameters.AddWithValue("@department", employee.Department);
                sqlCmd.Parameters.AddWithValue("@address", employee.Address);
                sqlCmd.Parameters.AddWithValue("@state", employee.State);
                sqlCmd.Parameters.AddWithValue("@city", employee.City);
                sqlCmd.Parameters.AddWithValue("@zip", employee.Zip);


                int rowinserted = sqlCmd.ExecuteNonQuery();
            
                myConnection.Close();
            
        }
        //[ActionName("DeleteEmployee")]
        //public void DeleteEmployeeByID(int id)
        //{

        //    try
        //    {
        //        myConnection = Connect();
        //        sqlCmd.CommandType = CommandType.Text;
        //        sqlCmd.CommandText = "delete from document where Id=" + id + "";
        //        sqlCmd.Connection = myConnection;
        //        myConnection.Open();
        //        int rowDeleted = sqlCmd.ExecuteNonQuery();
        //    }
        //    catch (Exception error)
        //    { Console.WriteLine("error" + error); }
        //    finally
        //    {
        //        myConnection.Close();
        //    }
        //}
       
    }

}