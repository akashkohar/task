﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using UserDataLayer;
using UserDataLayer.Context;
using UsersInformation.Models;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Web;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using static System.Net.Mime.MediaTypeNames;
using System.Security.Claims;
using UserDataLayer.Entities;
using UsersInformation.Enums;
using System.Security.Cryptography;
using System.Text;
using UsersInformation.Provider;

namespace UsersInformation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    [EnableCors("Allow")]
    public class UsersController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UsersController(DataContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet("getUser")]
        [Authorize(Roles ="User, Admin")]
        public IActionResult GetUsers([FromQuery] TableModel model)
        {
            var pagedList = GetUsersData(model);
            var pagedListDto = PagedList<Users>.Clone(pagedList, pagedList.Items.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                email = x.Email,
                dateOfBirth = x.DOB,
                contactNumber = x.ContactNumber,
                userImageGuid = x.UserImageGuid
            }).ToList());
            return Ok(pagedListDto);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Users>> GetUsers(int id)
        {
            var users = await _context.Users.FindAsync(id);
            if (users == null)
            {
                return NotFound();
            }
            return users;
        }
      
        // PUT: api/Users/5
        [HttpPut("{id}")]
        [AllowAnonymous]
        [EnableCors("Allow")]
        public async Task<IActionResult> PutUsers(int id, Users users)
        {
            if (id != users.Id)
            {
                return BadRequest();
            }

            _context.Entry(users).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
     
        // POST: api/Users
        [HttpPost("PostUsers")]
        [AllowAnonymous]
        [EnableCors("Allow")]
        public async Task<ActionResult<Users>> PostUsers([FromForm]UsersModel newUser)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == newUser.Email);
            if (user == null)
            {
                Users collectUserData = new Users();
                collectUserData.Name = newUser.Name;
                collectUserData.Email = newUser.Email;
                collectUserData.DOB = newUser.DateOfBirth;
                collectUserData.ContactNumber = newUser.ContactNumber;
                collectUserData.UserImageName = newUser.UserImageName;
                collectUserData.UserImageGuid = Guid.NewGuid() + ".png";

                //var folderPath = Path.Combine(_hostingEnvironment.WebRootPath, "Images");
                //System.IO.File.WriteAllBytes(Path.Combine(folderPath, collectUserData.UserImageGuid), Convert.FromBase64String(newUser.UserImage));

                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images");
                var filePath = Path.Combine(uploads, collectUserData.UserImageGuid);

                using (var filestream = new FileStream(filePath, FileMode.Create))
                {
                     newUser.UserImage.CopyTo(filestream);
                }
                //var passwordBytes = Encoding.ASCII.GetBytes(newUser.Password);
                //var sha1 = new SHA1CryptoServiceProvider();
                //var sha1data = sha1.ComputeHash(passwordBytes);
                //collectUserData.Password = sha1data;
                collectUserData.Password = _SecureString.HASH_GENERATE(newUser.Password);
                _context.Users.Add(collectUserData);
                await _context.SaveChangesAsync();

                var role = _context.Roles.FirstOrDefault(x => x.Name == UserRoleType.User.ToString());
                var users = _context.Users.FirstOrDefault(x => x.Email == newUser.Email);
                if (role != null)
                {
                    var userRole = new UserInRole
                    {
                        UserId = users.Id,
                        UserRoleId = role.Id
                    };
                    _context.UserInRoles.Add(userRole);
                    _context.SaveChanges();
                }
                return CreatedAtAction("GetUsers", new { id = collectUserData.Id }, collectUserData);
            }
            return Ok();
         }
        public static string EncodePasswordToBase64(string password)
        {
            try
            {
                byte[] encData_byte = new byte[password.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        [AllowAnonymous]
        [EnableCors("Allow")]
        public async Task<ActionResult<Users>> DeleteUsers(int id)
        {
            var users = await _context.Users.FindAsync(id);
            if (users == null)
            {
                return NotFound();
            }

            _context.Users.Remove(users);
            await _context.SaveChangesAsync();

            return users;
        }
       
        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        public PagedList<Users> GetUsersData(TableModel tableModel)
        {
            var user = _context.Users;
            return PagedList<Users>.Create(user, tableModel);
        }

        
    }
}
