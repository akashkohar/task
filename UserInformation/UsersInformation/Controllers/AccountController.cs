﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using UserDataLayer.Context;
using UsersInformation.Models;
using UsersInformation.Provider;
using UsersInformation.Utils;

namespace UsersInformation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    [EnableCors("Allow")]
    public class AccountController : Controller
    {
        private readonly DataContext _context;
        private IConfiguration _config;

        public AccountController(DataContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }
        [HttpPost("Create")]
        public IActionResult CreateToken([FromForm]LoginModel userData)
        {
            IActionResult response = Unauthorized();

            var user = Authenticate(userData);
            if (user)
            {
                var tokenString = BuildToken(userData);
                response = Ok(new { token = tokenString });
            }
            return response;
        }

        private string BuildToken(LoginModel userData)
        {
            var userId = _context.Users.FirstOrDefault(x => x.Email == userData.Email)?.Id ?? 0;
            var roleId = _context.UserInRoles.FirstOrDefault(x => x.UserId == userId)?.UserRoleId ?? 0;
            var roleName = _context.Roles.Where(x => x.Id == roleId).Select(x => x.Name).ToArray();
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(AppSettings.JwtToken.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {

                    new Claim(ClaimTypes.Role, string.Join(",", roleName[0])),
                    new Claim(ClaimTypes.Email, userData.Email),
                    
                }),
                Audience = AppSettings.JwtToken.Audience,
                Issuer = AppSettings.JwtToken.Issuer,
                Expires = DateTime.UtcNow.AddMonths(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            

            return tokenHandler.WriteToken(token);

        }
        private Boolean Authenticate(LoginModel login)
        {
           Boolean isAuthenticate = false;
            var checkUser = _context.Users.FirstOrDefault(x => x.Email == login.Email);
            var pass = _SecureString.HASH_GENERATE(login.Password);
            if (checkUser.Password == pass)
            {
                isAuthenticate = true;
            }
            return isAuthenticate;
        }

        
        }
    }
