﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersInformation.Utils
{
    public class AppSettings
    {
        public static string ClientAppUrl => Environment.GetEnvironmentVariable("ClientAppUrl");

        public static string DefaultConnectionString => Environment.GetEnvironmentVariable("DefaultConnectionString");

        public static class JwtToken
        {
            public static string Issuer => Environment.GetEnvironmentVariable("JwtTokenIssuer");

            public static string Audience => Environment.GetEnvironmentVariable("JwtTokenAudience");

            public static string Secret => Environment.GetEnvironmentVariable("JwtTokenSecret");
        }

        public static class Smtp
        {
            public static string Host => Environment.GetEnvironmentVariable("SmtpHost");

            public static int Port => Convert.ToInt32(Environment.GetEnvironmentVariable("SmtpPort"));

            public static string Username => Environment.GetEnvironmentVariable("SmtpUsername");

            public static string Password => Environment.GetEnvironmentVariable("SmtpPassword");

            public static string FromName => Environment.GetEnvironmentVariable("SmtpFromName");

            public static string FromEmail => Environment.GetEnvironmentVariable("SmtpFromEmail");
        }

        public static class Aws
        {
            public static string PublicBucketName => Environment.GetEnvironmentVariable("AWSPublicBucketName");

            public static string PrivateBucketName => Environment.GetEnvironmentVariable("AWSPrivateBucketName");

            public static string BucketUrl => Environment.GetEnvironmentVariable("AWSBucketUrl");

            public static string AccessKeyId => Environment.GetEnvironmentVariable("AWSAccessKeyId");

            public static string SecretKey => Environment.GetEnvironmentVariable("AWSSecretKey");
        }
    }
}

