﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersInformation.Enums
{
    public enum UserRoleType
    {
        User = 1,
        Admin = 2
    }
}
