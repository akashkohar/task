﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersInformation.Models
{
    public class UsersModel
    {
     public   UsersModel() {
        }
      public  UsersModel(UsersModel users)
        {
            Name = users.Name;
            Email = users.Email;
            ContactNumber = users.ContactNumber;
            DateOfBirth = users.DateOfBirth;
            UserImageName = users.UserImageName;
        }
        public string Name { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }

        public string UserImageName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public long ContactNumber { get; set; }
        // public List<IFormFile> UserImage { get; set; }
        public IFormFile UserImage {get;set;}
    }
}
