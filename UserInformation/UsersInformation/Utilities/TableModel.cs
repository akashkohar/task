﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersInformation.Models
{
    public class TableModel
    {
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 3;

        
    }
}
