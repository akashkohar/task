﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsersInformation.Models
{
    public class PagedList<T> where T : class
    {
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public List<T> Items { get; set; }


        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            Items = items;
            TotalCount = count;
            PageSize = pageSize;
            CurrentPage = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public static PagedList<T> Create(IQueryable<T> source, TableModel model)
        {
            var count = source.Count();
            
            var items = source.Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();
            return new PagedList<T>(items, count, model.Page, model.PageSize);
        }

        public static PagedList<V> Clone<V>(PagedList<T> pagedList, List<V> items) where V : class
        {
            return new PagedList<V>(items, pagedList.TotalCount, pagedList.CurrentPage, pagedList.PageSize);
        }

    }
}
