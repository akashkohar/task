﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace UsersInformation.Provider
{
    public class _SecureString
    {
        public static SecureString ENCODING(string _string)
        {
            SecureString secure = new SecureString();
            foreach (char c in _string)
            {
                secure.AppendChar(c);
            }
            return secure;
        }

        public static string DECODING(SecureString _secureString)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(_secureString);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }

        public static string HASH_GENERATE(string _inputString)
        {
            byte[] data = Encoding.UTF8.GetBytes(_inputString);
            using (SHA512 shaM = new SHA512Managed())
            {
                return Convert.ToBase64String(shaM.ComputeHash(data));
            }
        }
    }
}
