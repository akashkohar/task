﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserDataLayer.Entities
{
    public class UserInRole
    {
        public int Id { get; set; }
        public int UserRoleId { get; set; }
        public Roles UserRoles { get; set; }
        public int UserId { get; set; }
        public Users Users { get; set; }
    }
}
