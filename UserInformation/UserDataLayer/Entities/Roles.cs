﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserDataLayer.Entities
{
   public class Roles
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UserInRole> Role { get; set; }
    }
}
