﻿using System;
using System.Collections.Generic;
using UserDataLayer.Entities;

namespace UserDataLayer
{
    public class Users
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }
        
        public DateTime DOB { get; set; }
        public long ContactNumber { get; set; }
        public string Password { get; set; }
        public string  UserImageName { get; set;}
        public string UserImageGuid { get; set; }

        public ICollection<UserInRole> UserInRole  { get; set; }
    }

}
