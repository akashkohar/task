﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UserDataLayer.Entities;

namespace UserDataLayer.Context
{
   public class DataContext : DbContext
    {
        private string _connectionString;
        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);

            }
        }



        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        public virtual DbSet<Users> Users { get; set; }
        
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<UserInRole> UserInRoles { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Users>(data =>
            {
                data.ToTable("Users");

                data.HasKey(prime => prime.Id);

            });


            modelBuilder.Entity<Users>()
                        .HasMany<UserInRole>(x => x.UserInRole);


            modelBuilder.Entity<Roles>().HasMany<UserInRole>(x=>x.Role);
               
        }
        
    }
}
