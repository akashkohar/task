
//1. variable declaration
//var
var carName = "Volvo";
var carType = "Sedan"
var carInfo = carName + " " + carType;             //concatination
document.getElementById("demo").innerHTML = carInfo;  // passing the value to the html tag by the help of id

//let
//var keyword cannot have block scope but let keyword can have block scope

{
    let value = 2;
}
// "value" can NOT be used here outside blocks\

var value = 10;
// Here "value" is 10
{
    var value = 2;
    // Here "value" is 2
}
// Here "value" is 2

let value = 5;
for (let value = 0; value < 10; value++) {
    // some statements
}
// Here "value" is 5

//Redeclaring a var variable or let variable with let, in the same scope,
//or in the same block, is not allowed and vice versa


//const
//Variables defined with const behave like let variables, except they cannot be reassigned
//const also have scope like let
//It does NOT define a constant value. It defines a constant reference to a value.
var value = 10;
// Here "value" is 10
{
    const value = 2;
    // Here "value" is 2
}
// Here "value" is 10

//You can change the properties of a constant object
// You can create a const object,But you can NOT reassign a constant object
const car = { type: "Fiat", model: "500", color: "white" };

// You can change a property
car.color = "red";

// You can add a property
car.owner = "Johnson";


// You can create a constant array
const cars = ["Saab", "Volvo", "BMW"];

// You can change an element
cars[0] = "Toyota";

// You can add an element
cars.push("Audi");
//But you can NOT reassign a constant array
//Redeclaring or reassigning an existing var or let variable to const, 
//in the same scope, or in the same block, is not allowed

//data types

var value1 = "Volvo" + 16;//When adding a number and a string, JavaScript will treat the number as a string
var value1;           // Now "value" is undefined
value1 = 5;           // Now "value" is a Number
value1 = "John";      // Now "value" is a string


//Numbers can be written with, or without decimals
var value1 = 34.00;     // Written with decimals
var value2 = 34;        // Written without decimals
var exponatial = 123e5;      // 12300000
var exponatial = 123e-5;     // 0.00123

//boolean
var value1 = 5;
var value2 = 5;
var value3 = 6;
(value1 == value2)       // Returns true
    (value1 == value3)       // Returns false

//arrays
var cars = ["Saab", "Volvo", "BMW"];
//javascript objects
var person = { firstName: "Akash", lastName: "Kohar", age: 20, eyeColor: "blue" };
//typepf operator
typeof "Akash";        // Returns "string"
typeof 314;       // Returns "number"
//The typeof operator returns "object" for arrays because in JavaScript arrays are objects


//operators
//The assignment operator(=) assigns a value to a variable.

var value1 = 10;

var value1 = 5;
var value2 = 2;
var sum = value1 + value2;        //addition operator (+) adds numbers
var product = value1 * value2;    //multiplication operator (*) multiplies numbers

var txt1 = "Akash";
var txt2 = "Kohar";
var txt3 = txt1 + " " + txt2;//The + operator can also be used to add (concatenate) strings
//== equal to
//    === equal value and equal type
//!= not equal
//    !== not equal value or not equal type

//bitwise
//Any numeric operand in the operation is converted into a 32 bit number. 
//The result is converted back to a JavaScript number
//&	 AND	
//|    OR		 
//~    NOT
//^    XOR	
//<<   Zero fill left shift
//>>   Signed right shift
//>>>  Zero fill right shift

var value1 = 2;
var value2 = 5;
var power = value1 ** value2; // exponantiation operator returns power like in this example it returns a^b


//type conversion
//Converting Numbers to Strings
String(123)       // returns a string from a number literal 123
    (123).toString()
//Converting Booleans to Strings
String(false)      // returns "false"
true.toString()    // returns "true"
//Converting Dates to Strings

//Converting Strings to Numbers we can also use parseInt()
Number("3.14")    // returns 3.14
Number(" ")       // returns 0


//Converting Booleans to Numbers
//The global method Number() can also convert booleans to numbers

    Number(false)     // returns 0
Number(true)      // returns 1
date = new Date();
Number(date)        // converts date to numbers
//null and undefined comparision
var value1;
var value2 = null;
document.getElementById("demo").innerHTML = value1 == value2;//true
document.getElementById("demo").innerHTML = value1 === value2;//false

//switch case
var value = "0";
switch (value) {
    case 0:
        text = "False";
        break;
    case 1:
        text = "True";
        break;
    default:
        text = "No value found";
}


//Converting Arrays to Strings

var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo").innerHTML = fruits.toString()//Banana,Orange,Apple,Mango
// array methods
var value = fruits.pop();      // the value of "value" is "Mango" and removes Mango form array
var value = fruits.push("Kiwi");   //  the value of "value" is 5 and add KiWi to end of the array
var value = fruits.shift();    // the value of "value" is "Banana" and removes "Banana" from the array
var value = fruits.unshift("Lemon");    // Returns 5 and  in starting of the array
fruits[fruits.length] = "Kiwi";          // Appends "Kiwi" to fruits
fruits[0] = "Kiwi";        // Changes the first element of fruits to "Kiwi"
var removed = fruits.splice(2, 2, "Lemon", "Kiwi");// using this we can add and remove element from array


var myGirls = ["Komal", "Preeti"];
var myBoys = ["Akash", "Raju", "Amit"];
var myChildren = myGirls.concat(myBoys);   // Concatenates (joins) myGirls and myBoys


// string methods
var str = "Please locate where 'locate' occurs!";
var pos = str.indexOf("locate"); // The indexOf() method returns the index of (the position of) the first occurrence of a specified text in a string
var lenght = str.length;
var pos = str.lastIndexOf("locate"); // The lastIndexOf() method returns the index of the last occurrence of a specified text in a string and they both return -1 if index not found
var str = "Apple, Banana, Kiwi";
var res = str.slice(7, 13); // If a parameter is negative, the position is counted from the end of the string
var value = str.replace("Kiwi", "Mango"); // The replace() method replaces a specified value with another value in a string
var text = str.toUpperCase();
var text = str.toLowerCase();

var text1 = "Hello";
var text2 = "World   ";
var text3 = text1.concat(" ", text2); //  For merging both the string values and seperated by space
var trimed = text2.trim();


//number methods
var value = 9.656;
value.toExponential(2);//converting to exponantial form
value.toFixed(0); // 9
value.toPrecision(2); // returns a string, with a number written with a specified length
value.valueOf(); // returns x's value

// date get methods
// getFullYear()	Get the year as a four digit number(yyyy)
// getMonth()	Get the month as a number(0 - 11)
// getDate()	Get the day as a number(1 - 31)
// getHours()	Get the hour(0 - 23)
// getMinutes()	Get the minute(0 - 59)
// getSeconds()	Get the second(0 - 59)
// getMilliseconds()	Get the millisecond(0 - 999)
// getTime()	Get the time(milliseconds since January 1, 1970)
// getDay()	Get the weekday as a number(0 - 6)
// Date.now()	Get the time.ECMAScript 5.
var date = new Date();
date.getDate();
date.getDay();
date.getFullYear;
date.getHours();
date.getMonth();
date.setFullYear(2020);
date.setMonth(3);
date.setHours(1);


// date set methods
// setDate()	Set the day as a number(1 - 31)
// setFullYear()	Set the year(optionally month and day)
// setHours()	Set the hour(0 - 23)
// setMilliseconds()	Set the milliseconds(0 - 999)
// setMinutes()	Set the minutes(0 - 59)
// setMonth()	Set the month(0 - 11)
// setSeconds()	Set the seconds(0 - 59)
// setTime()	Set the time(milliseconds since January 1, 1970)


// JSON
// JSON is often used when data is sent from a server to a web page
{//JSON array
    var persons = [
        { firstname: "Akash", lastname: "Kohar" },
        { firstname: "Ajit", lastname: "kumar" },
        { firstname: "Yash", lastname: "singh" }
    ];
    var obj = JSON.parse(person); // use the JavaScript built-in function JSON.parse() 
                                  // to convert the string into a JavaScript object
}


//JSON map
var persons = [
    { firstname: "Akash", lastname: "Kohar" },
    { firstname: "Ajit", lastname: "kumar" },
    { firstname: "Yash", lastname: "singh" }
];


function getFullName(item) {
    var fullname = [item.firstname, item.lastname].join(" ");
    return fullname;
}

function myFunction() {
    document.getElementById("demo").innerHTML = persons.map(getFullName); // It will return full name
}


//arrow function
hello = function () {
    return "Hello World!";
}
hello = () => {
    return "Hello World!";
}

// for loop
var cars = ['BMW', 'Volvo', 'Mini'];
var x;

for (x of cars) {
    document.write(x + "<br >");
}

//while loop
while (i < 10) {
    text += "The number is " + i;
    i++;
}

// do while loop
do {
    text += "The number is " + i;
    i++;
}
while (i < 10);

//Map
//Map is a collection of keyed data items, just like an Object. 
//But the main difference is that Map allows keys of any type.
let map = new Map();

map.set('1', 'str1');   // a string key
map.set(1, 'num1');     // a numeric key
map.set(true, 'bool1'); // a boolean key


var value = (map.get('1')); // 'str1'
var value = (map.get(1)); // 'num1'
var value = (map.get(true)); // 'bool1'


//Set
//A Set is a special type collection � �set of values� (without keys), where each value may occur only once.
//The main feature is that repeated calls of set.add(value) 
//with the same value don�t do anything.That�s the reason why each value appears in a Set only once.
let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

//multiple same values
set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

// set keeps only unique values
alert(set.size); // 3

for (let user of set) {
    alert(user.name); // John (then Pete and Mary)
}

//callback function
//In computer programming, a callback is a piece of executable code that is passed as an argument to other code,
//which is expected to call back(execute) the argument at some convenient time.

function functionOne(x) { alert(x); }//Function One takes in an argument and issues an alert with the x as it's argument.

function functionTwo(var1, callback) {  // Function Two takes in an argument and a function.
    callback(var1);
}

functionTwo(1, function (x) { alert(x); })

//Function Two then passes the argument it took in to the function it took in.

//Function One is the callback function in this case.