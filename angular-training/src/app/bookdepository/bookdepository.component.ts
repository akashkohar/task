import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-bookdepository',
  templateUrl: './bookdepository.component.html',
  styleUrls: ['./bookdepository.component.css']
})
export class BookdepositoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public data=[{id: 1,name:'Non-Medical',stock: 4},
{id: 2,name:'Medical',stock: 15},
{id: 3,name:'Commerce',stock: 11},
{id: 4,name:'Arts',stock: 9}
];
@Output() public updatedStock=new EventEmitter

valueSet(set){
  for(let valueAt in this.data)
  {
  if(this.data[valueAt].id==set[0].id)
  {
  this.data[valueAt].stock+=set[0].stock;
  
  }
  
  }
  
  }
}
