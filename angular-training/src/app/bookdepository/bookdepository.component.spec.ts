import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookdepositoryComponent } from './bookdepository.component';

describe('BookdepositoryComponent', () => {
  let component: BookdepositoryComponent;
  let fixture: ComponentFixture<BookdepositoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookdepositoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookdepositoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
