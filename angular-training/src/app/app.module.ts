import { EmployeeService } from './reactive-registration/services/employee.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
import{FormsModule} from '@angular/forms';
import { NgIfEvenDirective } from './directives/ngIfEven/ng-if-even.directive';
import { ChildComponent } from './child/child.component';
import { HighlightDirective } from './directives/atribute/highlight.directive';
import { YoungerChildComponent } from './younger-child/younger-child.component';
import { BookstoreComponent } from './bookstore/bookstore.component';
import { PipesComponent } from './pipes/pipes.component';
import { FactorialPipe } from './factorial.pipe';
import { BookdepositoryComponent } from './bookdepository/bookdepository.component';
import { RegistrationComponent } from './registration/registration.component';
import { ConvertMonthToYearPipe } from './convert-month-to-year.pipe';
import { ReactiveRegistrationComponent } from './reactive-registration/reactive-registration.component';
import{ReactiveFormsModule} from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import{HttpClientModule} from '@angular/common/http'
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import { ScheduledInterviewComponent } from './scheduled-interview/scheduled-interview.component';
import { ScheduledInterviewsDetailsComponent } from './ScheduledInterviewsDetails/scheduled-interviews-details/scheduled-interviews-details.component';
import { CandidateInreviewScheduleComponent } from './candidate-inreview-schedule/candidate-inreview-schedule.component';



@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    NgIfEvenDirective,
    ChildComponent,
    HighlightDirective,
    YoungerChildComponent,
    BookstoreComponent,
    PipesComponent,
    FactorialPipe,
    BookdepositoryComponent,
  
    RegistrationComponent,
  
    ConvertMonthToYearPipe,
  
    ReactiveRegistrationComponent,
  
    PageNotFoundComponent,
  
    ScheduledInterviewComponent,
  
    ScheduledInterviewsDetailsComponent,
  
    CandidateInreviewScheduleComponent,
  
    
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DlDateTimeDateModule,  // <--- Determines the data type of the model
    DlDateTimePickerModule,
    
   
    
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
  
})
export class AppModule {}
