import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  public data={ id: 1, member: { name: 'Akash' }};
  birthday = new Date(1988, 3, 15);
  number;
  sentence="ALL LETTERS ARE IN LOWER CASE";
}
