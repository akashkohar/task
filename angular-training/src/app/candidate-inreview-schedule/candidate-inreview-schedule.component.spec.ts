import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateInreviewScheduleComponent } from './candidate-inreview-schedule.component';

describe('CandidateInreviewScheduleComponent', () => {
  let component: CandidateInreviewScheduleComponent;
  let fixture: ComponentFixture<CandidateInreviewScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateInreviewScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateInreviewScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
