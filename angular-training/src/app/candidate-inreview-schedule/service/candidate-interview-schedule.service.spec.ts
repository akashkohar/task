import { TestBed } from '@angular/core/testing';

import { CandidateInterviewScheduleService } from './candidate-interview-schedule.service';

describe('CandidateInterviewScheduleService', () => {
  let service: CandidateInterviewScheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CandidateInterviewScheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
