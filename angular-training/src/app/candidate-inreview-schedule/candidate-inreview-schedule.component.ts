import { Details } from './../models/CandidateDetails';
import { Component, OnInit } from '@angular/core';
import { ScheduledInterviewsDetailsService } from '../ScheduledInterviewsDetails/scheduled-interviews-details/services/scheduled-interviews-details.service';

@Component({
  selector: 'app-candidate-inreview-schedule',
  templateUrl: './candidate-inreview-schedule.component.html',
  styleUrls: ['./candidate-inreview-schedule.component.css']
})
export class CandidateInreviewScheduleComponent implements OnInit {

  constructor( private recieveDataForEdit:ScheduledInterviewsDetailsService) { }

  ngOnInit(): void {
this.recievedData= this.recieveDataForEdit.sendDataForEdit();

  }
  recievedData:Details ;
editedDetails:Details;
  UpdateDetails(editedDetails){
    this.editedDetails=editedDetails;
    this.recieveDataForEdit.updateData(this.editedDetails.id,this.editedDetails).subscribe();

  }
}
