
import{AbstractControl,ValidatorFn, Validator} from '@angular/forms';
export function ExperienceValidator():ValidatorFn
{
    return (control:AbstractControl):{[key:string]:boolean} | null => {
        if(control.value<6 || control.value>120)
        {
            return {'NotAllowed':true}
        }
       
         return null;
    }
}