import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertMonthToYear'
})
export class ConvertMonthToYearPipe implements PipeTransform {

  transform(value, ...args: unknown[]): any {
   let year ;
   let month :number;
    if(value>0)
{
  year=Math.floor(value/12);
  month=value%12;
  if( month==0)
  {
    return year+"years";
  }
  if(year==0)
  {return month+"months"}
   if(month>0) {return year+"years"+' '+month+"months";}
}

else {return "";}




  }

}
