import { EmployeeService } from './services/employee.service';
import { Login } from './../models/login';
import { FormGroup, FormControl,FormArray,Validators ,FormBuilder} from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import{ExperienceValidator} from './../customValidators/checkexperience';

@Component({
  selector: 'app-reactive-registration',
  templateUrl: './reactive-registration.component.html',
  styleUrls: ['./reactive-registration.component.css']
})

export class ReactiveRegistrationComponent implements OnInit {
  @ViewChild('ragistrationForm') formValues;
  constructor( private _employeeService:EmployeeService) { }


   registrationForm= new FormGroup({
     id:new FormControl(''),
    firstName:new FormControl('',[Validators.required ,Validators.minLength(2)]),
   lastName:new FormControl('',Validators.required),
   email:new FormControl('',Validators.required),
   phone:new FormControl('',[Validators.required ,Validators.minLength(10)]),
   dateOfBirth:new FormControl('',Validators.required),
   experience:new FormControl('',[Validators.required,ExperienceValidator()]),
   
   
});


selectedDate;
  ngOnInit(): void { this._employeeService.getemployee().subscribe(data=>this.dataToTable=data)}

 
array=[];
  data=-1;
  dataToTable=[];
  showTable:boolean=true;
  
  dataTable()
  {
    if(this.registrationForm.value.id==this.data)
   {
   this._employeeService.updateData(this.registrationForm.value.id,this.registrationForm.value).subscribe();
   this.data=-1; 
   this.dataToTable.splice(this.data, 1);
   this.dataToTable.splice(this.index, 0,this.registrationForm.value );
   this.index=-1;
  }
    else{
      this.getData();

      this.dataToTable.push(this.registrationForm.value);
      delete this.registrationForm.value.id;
      
      this._employeeService.createPost(this.registrationForm.value).subscribe();
    
    }
    
   this.registrationForm.reset();
   
   
  }
getData()
{
  this.showTable=true;
  this._employeeService.getemployee().subscribe(data=>this.dataToTable=data)
  window.location.reload()
}
  deleteRow(id,index) {  
    this._employeeService.deleteData(id).subscribe();
    //this.getData();
    this.dataToTable.splice(index, 1);
      } 
      index=-1;
  setSelectedEmployee(dataToEdit,index,id)
    {
      this.index=index;
      dataToEdit.id=id;
      this.data=dataToEdit.id;
      this.registrationForm.setValue(dataToEdit);
   
    }



}
