import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IEmployee } from '../../employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  constructor(private _http:HttpClient) { }
private _url="https://localhost:5001/api"
httpOption={
  headers:new HttpHeaders({'Content-Type': 'application/json'})
}
  getemployee(): Observable<any[]> {
    return this._http.get<any[]>(this._url + "/CandidatesDatas/GetCandidatesDatas");
  }
  createPost(postData){
    return this._http.post<JSON>(this._url + '/CandidatesDatas/PostCandidatesData', postData, this.httpOption);
  }
  deleteData(id){
    return this._http.delete<any[]>(this._url+"/CandidatesDatas/DeleteCandidatesData/"+id)
  }

  updateData(id,putData){
    return this._http.put<JSON>(this._url+"/CandidatesDatas/PutCandidatesData/"+id,putData)
  }
}
