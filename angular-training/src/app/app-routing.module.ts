import { CandidateInreviewScheduleComponent } from './candidate-inreview-schedule/candidate-inreview-schedule.component';
import { ScheduledInterviewsDetailsComponent } from './ScheduledInterviewsDetails/scheduled-interviews-details/scheduled-interviews-details.component';
import { ScheduledInterviewComponent } from './scheduled-interview/scheduled-interview.component';
import { YoungerChildComponent } from './younger-child/younger-child.component';
import { ReactiveRegistrationComponent } from './reactive-registration/reactive-registration.component';
import { RegistrationComponent } from './registration/registration.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';



const routes:Routes=[
  {path:'template',component:RegistrationComponent},
  {path:'reactive',component:ReactiveRegistrationComponent},
  {path:'services',component:YoungerChildComponent},
  {path:'scheduleinterview',component:ScheduledInterviewComponent},
  {path:'scheduleinterviewdetails',component:ScheduledInterviewsDetailsComponent},
  {path:'candidateinterviewschedule',component:CandidateInreviewScheduleComponent},
  {path:'',redirectTo:'/',pathMatch:'full'},
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const ROUTINGCOMPONENTS=[RegistrationComponent,ReactiveRegistrationComponent];