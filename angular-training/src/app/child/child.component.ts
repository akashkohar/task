import { Component, OnInit, Input,EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor() { }
@Input() parentData;
@Output() public childEvent=new EventEmitter

  ngOnInit(): void {
  }
  fireEvent(){

    this.childEvent.emit('i miss you');
  }
 

}
