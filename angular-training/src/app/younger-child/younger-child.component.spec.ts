import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoungerChildComponent } from './younger-child.component';

describe('YoungerChildComponent', () => {
  let component: YoungerChildComponent;
  let fixture: ComponentFixture<YoungerChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoungerChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoungerChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
