import { EmployeeService } from '../reactive-registration/services/employee.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-younger-child',
  templateUrl: './younger-child.component.html',
  styleUrls: ['./younger-child.component.css']
})
export class YoungerChildComponent implements OnInit {
  public employee=[];
  constructor(private _employeeService:EmployeeService) { }
@Input() public brotherData;
  ngOnInit(): void {  this._employeeService.getemployee().subscribe(data=>this.employee=data);
  }

}
