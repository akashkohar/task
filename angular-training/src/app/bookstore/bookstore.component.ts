import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-bookstore',
  templateUrl: './bookstore.component.html',
  styleUrls: ['./bookstore.component.css']
})
export class BookstoreComponent implements OnInit {

  constructor() { }

@Input() bookId;
@Input() stockAvailable;
@Output() public updatedStock=new EventEmitter
 

ngOnInit(): void {
  }
  
 public sendUpdate;


ButtonClass()
{
if(this.stockAvailable>10)
{
  return "btn btn-success";
}
else{
  return "btn btn-danger";
}

}
sendData(valueInput)


{
    this.sendUpdate=[{id: this.bookId,stock:valueInput.valueAsNumber}];
  this.updatedStock.emit(this.sendUpdate);
}

}
