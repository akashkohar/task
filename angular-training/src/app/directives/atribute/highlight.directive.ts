import { element } from 'protractor';
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
@Input('appHighlight') data;
  constructor( private e1:ElementRef) {
}


@HostListener('blur') onblur(){
  if(isNaN(this.data.value)){
this.e1.nativeElement.style.color="red";}
else{
  this.e1.nativeElement.style.color="green";
  }
}


}
