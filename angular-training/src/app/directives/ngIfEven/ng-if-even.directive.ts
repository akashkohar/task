import { Directive , Input , TemplateRef, ViewContainerRef}  from '@angular/core';

@Directive({
  selector: '[ngIfEven]'
})
export class NgIfEvenDirective {

  constructor(
    private templateRef:TemplateRef<any> , 
    private vcRef:ViewContainerRef) { }


  @Input() set  ngIfEven(condition)
  {
  if(condition%2==0)
  {this.vcRef.clear();
    this.vcRef.createEmbeddedView(this.templateRef);}
  else{
this.vcRef.clear();
  }
  
  }
}
