import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScheduledInterviewService {

  constructor(private _http:HttpClient) { }

  private _url="https://localhost:5001/api"
httpOption={
  headers:new HttpHeaders({'Content-Type':'application/json'})
}
  getemployee():Observable<any[]> {
    return this._http.get<any[]>(this._url+"/ScheduledInterviews/GetInterviewSchedule");
  }
  createPost(postData){
    return this._http.post<JSON>(this._url+"/ScheduledInterviews/PostScheduledInterviews",postData,this.httpOption)
  }
  deleteData(id){
    return this._http.delete<any[]>(this._url+"/ScheduledInterviews/DeleteScheduledInterviews/"+id)
  }

  updateData(id,putData){
    return this._http.put<JSON>(this._url+"/ScheduledInterviews/PutScheduledInterviews/"+id,putData)
  }
  check(){
    return this._http.get<any[]>(this._url+"/ScheduledInterviews/GetNameFromId");
  }

}
