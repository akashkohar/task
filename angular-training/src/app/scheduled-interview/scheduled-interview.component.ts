import { ScheduledInterviewService } from './service/scheduled-interview.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-scheduled-interview',
  templateUrl: './scheduled-interview.component.html',
  styleUrls: ['./scheduled-interview.component.css']
})
export class ScheduledInterviewComponent implements OnInit {

  constructor(private _employeeService:ScheduledInterviewService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void { this._employeeService.getemployee().subscribe(data=>this.dataToTable=data);
  
    this._employeeService.check().subscribe(data=>this.selectData=data)
    this.curPage=1;
    this.itempage=2;
    this.totalrecord=3;
  }

  scheduledInterview= new FormGroup({
    id:new FormControl(''),
    candidateId:new FormControl('',Validators.required),
    stratDateTime:new FormControl('',Validators.required),
    endDateTime:new FormControl(''),
  interviewerName:new FormControl('')
  
});
selectData=[];

array=[];
  data=-1;
  dataToTable=[];
  showTable:boolean=true;
  
  dataTable()
  {
    if(this.scheduledInterview.value.id==this.data)
   {
   this._employeeService.updateData(this.scheduledInterview.value.id,this.scheduledInterview.value).subscribe();
   this.data=-1; 
   this.dataToTable.splice(this.index, 1);
   this.dataToTable.splice(this.index, 0,this.scheduledInterview.value );
   this.index=-1;
  }
    else{
      this.getData();
      this.dataToTable.push(this.scheduledInterview.value);
      delete this.scheduledInterview.value.id;
      
      this._employeeService.createPost(this.scheduledInterview.value).subscribe();
      

    }
    
   this.scheduledInterview.reset();
   }

  totalrecord:number=3;
  curPage:number=1;
  itempage:number=2;
getData()
{this.spinner.show();
  this.showTable=true;
  this._employeeService.getemployee().subscribe(data=>this.dataToTable=data)
  window.location.reload();
  this.spinner.hide();
}
  deleteRow(id,index) {  
    this._employeeService.deleteData(id).subscribe();
    //this.getData();
    this.dataToTable.splice(index, 1);
      } 
      index=-1;
  setSelectedEmployee(dataToEdit,index)
    {
      this.index=index;
      dataToEdit.id=dataToEdit.id;
      this.data=dataToEdit.id;
      this.scheduledInterview.setValue(dataToEdit);
   
    }



}



