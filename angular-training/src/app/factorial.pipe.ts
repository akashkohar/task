import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'factorial',
  pure:false
})
export class FactorialPipe implements PipeTransform {

  transform(value, ...args: unknown[]): number {
    let fact=1;
    for(let i=value.valueAsNumber;i>1;i--)
    {
      fact=fact*i;
    }
    return fact;
  }

}
