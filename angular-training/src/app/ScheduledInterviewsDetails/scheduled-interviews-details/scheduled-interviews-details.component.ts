import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { ScheduledInterviewsDetailsService } from './services/scheduled-interviews-details.service';
import { Details } from 'src/app/models/CandidateDetails';

@Component({
  selector: 'app-scheduled-interviews-details',
  templateUrl: './scheduled-interviews-details.component.html',
  styleUrls: ['./scheduled-interviews-details.component.css']
})
export class ScheduledInterviewsDetailsComponent implements OnInit {

  constructor(private _employeeService:ScheduledInterviewsDetailsService) { 


  }

  ngOnInit(): void {this._employeeService.getDetails().subscribe(data=>this.dataToTable=data);
  
  }
dataToTable :Details[];
showTable:boolean=true;

deleteRow(interviewId,index){
  this._employeeService.deleteData(interviewId).subscribe();
  this.dataToTable.splice(index, 1);
}

setSelectedEmployee(dataForEdit: Details) {
  this._employeeService.DataForEdit(dataForEdit);
  
}


}
