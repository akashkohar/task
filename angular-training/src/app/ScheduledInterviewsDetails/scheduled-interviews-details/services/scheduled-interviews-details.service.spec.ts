import { TestBed } from '@angular/core/testing';

import { ScheduledInterviewsDetailsService } from './scheduled-interviews-details.service';

describe('ScheduledInterviewsDetailsService', () => {
  let service: ScheduledInterviewsDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduledInterviewsDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
