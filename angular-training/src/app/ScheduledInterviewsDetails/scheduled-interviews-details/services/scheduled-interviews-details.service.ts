import { Details } from './../../../models/CandidateDetails';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ScheduledInterviewsDetailsService {

  constructor(private _http:HttpClient) { }
private _EditData:Details;

  private _url="https://localhost:5001/api"
  getDetails():Observable<Details[]> {
    return this._http.get<Details[]>(this._url+"/SceduledInterviewsDetails/GetScheduledInterviewsDetails");
  }

  deleteData(interviewId){
    return this._http.delete<any[]>(this._url+"/SceduledInterviewsDetails/DeleteCandidatesData/"+interviewId)
  }
DataForEdit(dataForEdit:Details){
this._EditData=dataForEdit;
}
sendDataForEdit(){
  return this._EditData;
}

updateData(id,putData){
  return this._http.put<Details>(this._url+"/SceduledInterviewsDetails/PutScheduledInterviewDetails/"+id,putData)
}


}
