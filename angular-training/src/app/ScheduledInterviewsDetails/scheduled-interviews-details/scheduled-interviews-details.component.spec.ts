import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduledInterviewsDetailsComponent } from './scheduled-interviews-details.component';

describe('ScheduledInterviewsDetailsComponent', () => {
  let component: ScheduledInterviewsDetailsComponent;
  let fixture: ComponentFixture<ScheduledInterviewsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduledInterviewsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduledInterviewsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
