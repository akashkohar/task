import { UsersService } from './../../UsersInformation/services/users.service';
import { UserRegistraionComponent } from './../../UsersInformation/Components/user-registraion/user-registraion.component';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input, Output } from '@angular/core';
import { ImageTransform, ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnInit {

 @Input() public imageChangedEvent;
  croppedImage: any = '';
  model: NgbDateStruct;
  canvasRotation = 0;
  rotation = 0;
  scale = 1;
  showCropper = false;
  containWithinAspectRatio = false;
  transform: ImageTransform = {};
  constructor(private userService: UsersService ) { }

  ngOnInit(): void {
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = base64ToFile(event.base64);
    this.userService.CroppedImage = this.croppedImage;
    }

  imageLoaded() {
    this.showCropper = true;
  }

}
