import { AdminComponent } from './UsersInformation/Components/admin/admin.component';
import { AuthGuard, AdminGuard } from './auth/guards/auth.guard';
import { UserLoginComponent } from './UsersInformation/Components/user-login/user-login.component';
import { ImageCropperComponent } from './shared-components/image-cropper/image-cropper.component';
import { UserRegistraionComponent } from './UsersInformation/Components/user-registraion/user-registraion.component';
import { UsersInformationComponent } from './UsersInformation/Components/users-information/users-information.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';



const routes: Routes = [{path: 'UserDetails', component: UsersInformationComponent, canActivate: [ AuthGuard ] },
                        {path: 'Registraion', component: UserRegistraionComponent},
                        {path: 'Cropper', component: ImageCropperComponent},
                        {path: 'Login', component: UserLoginComponent},
                        {path: 'Admin', component: AdminComponent, canActivate: [ AdminGuard ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
