import '@angular/compiler';
import { AuthGuard, AdminGuard } from './auth/guards/auth.guard';
import { UserLoginComponent } from './UsersInformation/Components/user-login/user-login.component';
import { UsersService } from './UsersInformation/services/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersInformationComponent } from './UsersInformation/Components/users-information/users-information.component';
import { NgbPaginationModule, NgbAlertModule, NgbNavConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserRegistraionComponent } from './UsersInformation/Components/user-registraion/user-registraion.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ImageCropperComponent } from './shared-components/image-cropper/image-cropper.component';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './UsersInformation/Components/admin/admin.component';
import { AuthIntercepter } from './auth/intercepter/auth-intercepter';


@NgModule({
  declarations: [
    AppComponent,
    UsersInformationComponent,
    UserRegistraionComponent,
    ImageCropperComponent,
    UserLoginComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbPaginationModule,
    NgbAlertModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    BrowserAnimationsModule,
    ImageCropperModule,
    NgxMaskModule.forRoot(),
    CommonModule,
  ],
  providers: [
    UsersService,
    AuthGuard,
    AdminGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthIntercepter,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

