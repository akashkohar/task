import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree {
      if ( localStorage.getItem('token') == null )
      {
        this.router.navigate(['/Login']);
      }
      else {
      const decoded = jwt_decode(localStorage.getItem('token'));
      if (decoded.role === 'User' || decoded.role === 'Admin') {
        return true;
    }
    else {
      alert('Uauthereized Access');
      this.router.navigate(['/Login']);
      return false;
    }
  }
 }
}
@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree {
      if ( localStorage.getItem('token') == null )
      {
        this.router.navigate(['/Login']);
      }
      else {
      const decoded = jwt_decode(localStorage.getItem('token'));
      if (decoded.role === 'Admin') {
        return true;
    }
    else{
      alert('Uauthereized Access');
      this.router.navigate(['/Login']);
      return false;
    }
  }
  }
}
