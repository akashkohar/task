export class TableModel {

  pageSize: number;

  private page: number;
  get Page() {
    return this.page;
  }
  set Page(value: number) {
    this.page = value;

  }

  constructor() {

    this.page = 1;
    this.pageSize = 5;

  }

  public toQueryString = () => `&pageSize=${this.pageSize}&Page=${this.page}`;
}
