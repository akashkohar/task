export class PagedList<T> {
    items: T[];
    totalCount: number;
    totalPages: number;
}
