import { FormControl } from '@angular/forms';
import {PagedList} from './../../Models/pageList';
import {User} from './../Models/Users';
import {environment} from './../../../environments/environment';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpEvent, HttpHeaders} from '@angular/common/http';



const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private croppedImage;
  get CroppedImage() {
    return this.croppedImage;
  }
  set CroppedImage(incommingCroppedImage){
    this.croppedImage = incommingCroppedImage;
  }

  constructor(private httpClient: HttpClient) {}

  getUsers(Page: number, pageSize: number) {
    return this.httpClient.get < PagedList < User >> (`${environment.apiUrl}/api/users/getUser?pageSize=
    ${pageSize}&Page=${Page}` );
  }
  getRequestedImage(userGuid) {
    return this.httpClient.get < any > (`${environment.apiUrl}/api/users/downloadfile?userGuid=${userGuid}`);
  }
  createNewUser(postUserData, selectedFileName, croppedImage) {
    const formData = new FormData();
    formData.append('Name', postUserData.value.name);
    formData.append('Email', postUserData.value.email);
    formData.append('DateOfBirth', postUserData.value.dateOfBirth.day + '/'
                     +  postUserData.value.dateOfBirth.month + '/' + postUserData.value.dateOfBirth.year + ' 00:00:00.0000000');
    formData.append('ContactNumber', postUserData.value.contactNumber);
    formData.append('UserImageName', selectedFileName);
    formData.append('UserImage', croppedImage);
    formData.append('Password', postUserData.value.password);
    return this.httpClient.post < any > (`${environment.apiUrl}/api/users/PostUsers`, formData);
  }
  uploadImage(userImage) {
      const formData = new FormData();
      formData.append('file', userImage);
      return this.httpClient.post < any > (`${environment.apiUrl}/api/users/UploadImage`, formData);
  }
  deleteUserData(id) {
    return this.httpClient.delete < User > (`${environment.apiUrl}/api/users/` + id);
  }

  updateUserData(id, putData) {
    return this.httpClient.put < JSON > (`${environment.apiUrl}/api/users` + id, putData);
  }
userLoginData(userData){
  const formData = new FormData();
  formData.append('Email', userData.value.email);
  formData.append('Password', userData.value.password);
  return this.httpClient.post < any > (`${environment.apiUrl}/api/Account/Create`, formData);
}
}
