export class User {
  Name: string;
  Email: string;
  UserImageName: string;
  UserGuid: string;
  DateOfBirth: string;
  ContactNumber: number;
}
export class Formats {
  static readonly phone: '00-0000-0000';
}
