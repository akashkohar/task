import { UsersService } from './../../services/users.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
  isAuthenticated;
  constructor(private userLogin: UsersService, private router: Router) { }

  ngOnInit(): void {
  }
  userLoginAuthentication(formValue) {
   this.userLogin.userLoginData(formValue).subscribe(data => {
    localStorage.setItem('token', data.token);
    // tslint:disable-next-line: no-unused-expression
    this.router.navigate(['/']);
   },
   (err: HttpErrorResponse) => {

   });
  }

}
