import { environment } from './../../../../environments/environment';
import {TableModel} from './../../../Models/tableModel';
import {startWith, map} from 'rxjs/operators';
import {User} from './../../Models/Users';
import {Component, OnInit, PipeTransform, ViewChild} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {UsersService} from '../../services/users.service';
import {Observable, Subscription} from 'rxjs';
import {DecimalPipe} from '@angular/common';
import {HttpEvent} from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';


@Component({
  selector: 'app-users-information',
  templateUrl: './users-information.component.html',
  styleUrls: ['./users-information.component.scss']
})
export class UsersInformationComponent implements OnInit {

  userInfo: User[];
  totalRecords: number;
  tableModel: TableModel;
  imagepath: string;
  genuineUrl;
  userImage: File;


  constructor(private userService: UsersService, private router: Router) { }
  ngOnInit(): void {
    this.tableModel = new TableModel();
    this.getUserData();
  }
 getUserImage(UserGuid){
    this.userService.getRequestedImage(UserGuid).subscribe(data => this.userImage = data );
    return this.userImage;
 }

 getUserData() {
    this.userService.getUsers(this.tableModel.Page, this.tableModel.pageSize)
      .subscribe(data => {
        this.userInfo = data.items,
        this.totalRecords = data.totalCount;
      });
  }

  deleteRow(id, index) {
    this.userService.deleteUserData(id).subscribe();
    this.userInfo.splice(index, 1);
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/Login']);
  }
}
