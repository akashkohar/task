import { ImageCropperComponent } from './../../../shared-components/image-cropper/image-cropper.component';


import {TableModel} from './../../../Models/tableModel';
import {UsersService} from './../../services/users.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import { User } from '../../Models/Users';
import { NgbModalConfig, NgbModal, NgbDateStruct, NgbInputDatepickerConfig, NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { ImageCroppedEvent, ImageTransform, base64ToFile, Dimensions } from 'ngx-image-cropper';

@Component({
  selector: 'app-user-registraion',
  templateUrl: './user-registraion.component.html',
  styleUrls: ['./user-registraion.component.scss']
})
export class UserRegistraionComponent implements OnInit {

@ViewChild('ragistrationForm') formValues;
  userInfo = [];
  totalRecords: number;
  elementPerpage: number;
  currentPage: number;
  tableModel: TableModel;
  selectedFile: File;
  imageChangedEvent;
  croppedImage: any = '';
  model: NgbDateStruct;

  constructor(private userService: UsersService, config: NgbModalConfig, private modalService: NgbModal,
              configs: NgbInputDatepickerConfig, calendar: NgbCalendar) {
    this.elementPerpage = 5;
    this.currentPage = 1;
    config.backdrop = 'static';
    config.keyboard = false;
    configs.minDate = {year: 1900, month: 1, day: 1};
    configs.maxDate = {year: 2099, month: 12, day: 31};
    configs.outsideDays = 'hidden';
    configs.markDisabled = (date: NgbDate) => calendar.getWeekday(date) >= 6;
    configs.autoClose = 'outside';
    configs.placement = ['top-left', 'top-right'];
    }


  ngOnInit(): void {
  }

  open(content) {
    this.modalService.open(content);
  }
  postNewUser(formValue) {
    this.croppedImage = this.userService.CroppedImage;
    this.userService.createNewUser(formValue, this.selectedFile.name, this.croppedImage).subscribe();
    this.selectedFile = null;
    this.formValues.resetForm();
 }

  selectFile(event, content) {
    if (event.target.files[0]) {
      this.imageChangedEvent = event;
      this.selectedFile = event.target.files[0];
      this.modalService.open(content);
    }
  }
  uploadButtonClass() {
    if (this.selectedFile) {
      return 'btn btn-success';
    }
    else {
      return 'btn btn-danger';
    }
  }

}
