import { AuthGuard, AdminGuard } from './../auth/guards/auth.guard';
import { UsersService } from './../UsersInformation/services/users.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { AdminRoutingModule } from './admin-routing-module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthIntercepter } from '../auth/intercepter/auth-intercepter';



@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthIntercepter,
      multi: true
    },
    UsersService,
    AuthGuard,
    AdminGuard,
  ],
})
export class AdminModule { }
