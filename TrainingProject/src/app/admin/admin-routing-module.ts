import { AdminComponent } from './admin/admin.component';
import { AdminGuard } from './../auth/guards/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {path: 'Admin', component: AdminComponent, canActivate: [ AdminGuard ] },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}