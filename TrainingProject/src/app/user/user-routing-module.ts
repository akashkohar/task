import { UserLoginComponent } from './user-login/user-login.component';
import { UserInformationComponent } from './user-information/user-information.component';
import { AuthGuard } from './../auth/guards/auth.guard';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {path: 'UserDetails', component: UserInformationComponent, canActivate: [ AuthGuard ] },
    {path: 'Registraion', component: UserRegistrationComponent},
    {path: 'Login', component: UserLoginComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class UserRoutingModule {

}