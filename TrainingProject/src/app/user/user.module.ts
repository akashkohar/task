import { AuthGuard, AdminGuard } from './../auth/guards/auth.guard';
import { UsersService } from './../UsersInformation/services/users.service';
import { ImageCropperComponent } from './../shared-components/image-cropper/image-cropper.component';
import { NgxMaskModule } from 'ngx-mask';
import { ImageCropperModule, ImageCroppedEvent } from 'ngx-image-cropper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { UserRegistrationComponent } from './user-registration/user-registration.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserInformationComponent } from './user-information/user-information.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserRoutingModule } from './user-routing-module';
import { AuthIntercepter } from '../auth/intercepter/auth-intercepter';

@NgModule({
  declarations: [
    UserLoginComponent,
    UserInformationComponent,
    UserRegistrationComponent,
    ImageCropperComponent
  ],
  imports: [
    BrowserModule,
    UserRoutingModule,
    HttpClientModule,
    NgbPaginationModule,
    NgbAlertModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    BrowserAnimationsModule,
    ImageCropperModule,
    NgxMaskModule.forRoot(),
    CommonModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthIntercepter,
      multi: true
    },
    UsersService,
    AuthGuard,
    AdminGuard,
  ],
})
export class UserModule { }
