﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Organiser
{/// <summary>
/// Display class is the class cosnsist of main funtion . 
/// </summary>
    class Display
    {
        private int _typeOfParty;

        public int TypeOfParty
        {
            get => _typeOfParty;
            set => _typeOfParty = value;
        }

        public static void Main(string[] args)
        {
            Display Result = new Display();
            Anversary CallAnversary = new Anversary();
            Birthday CallBirthday = new Birthday();
            BabyShower CallbabyShower = new BabyShower();
            Party TakeInput = new Party();
            
            try
            {
                Console.WriteLine("we organise follwing types of party");
                Console.WriteLine("(1)Birthday party");
                Console.WriteLine("(2)Anversary party");
                Console.WriteLine("(3)Baby Shower party");
                Console.WriteLine("choose any one [1/2/3]....");
                Result.TypeOfParty = Convert.ToInt32(Console.ReadLine());

            }
            catch(Exception e)
            {
               Console.WriteLine("please choose correct option");
               
            }

            

            switch (Result.TypeOfParty)
            {
                case 1:
                    {
                        CallBirthday.InputPersonalDetails();
                        CallBirthday.InputFunctionDetails();
                        CallBirthday.Packages();
                        
                        CallBirthday.ShowDetails();
                        CallBirthday.CustemerBookingId = CallBirthday.BookingId();
                        CallBirthday.Billing();

                        break;
                    }
                case 2:
                    {
                        CallAnversary.InputPersonalDetails();
                        CallAnversary.InputFunctionDetails();
                        CallAnversary.Packages();
                        CallAnversary.ShowDetails();
                        CallAnversary.CustemerBookingId = CallAnversary.BookingId();
                        CallAnversary.Billing();
                        break;
                    }
                case 3:
                    {
                        CallbabyShower.InputPersonalDetails();

                        CallbabyShower.InputFunctionDetails();
                        CallbabyShower.Packages();
                        
                        CallbabyShower.ShowDetails();
                        CallbabyShower.CustemerBookingId = CallbabyShower.BookingId();
                        CallbabyShower.Billing();
                        break;
                    }


            }


        }
    }
}
