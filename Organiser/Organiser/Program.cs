﻿using System;

namespace Organiser
{/// <summary>
/// Party class is the parent class which consist of all funtion and thier defination. 
/// </summary>
    class Party
    {
        private  string _customerName;
        private  int _customerContactNumber;
        private int _custemerBookingId;

        private  string _venue;
        private  string _date;
     
        private  int _typeOfPackage;
        private  int _noOfPeople;
        private  int _stopTime;
        private  int _startTime;
        private  int _amount;
        public  string Venue

        {
            get
            {
                return _venue;
            }
            set
            {
                _venue = value;

            }
        }
        public  string Date

        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;

            }
        }
        public  int StartTime

        {
            get
            {
                return _startTime;
            }
            set
            {
                _startTime = value;

            }
        }
        public int StopTime

        {
            get
            {
                return _stopTime;
            }
            set
            {
                _stopTime = value;

            }
        }

        public  int TypeOfPackage

        {
            get
            {
                return _typeOfPackage;
            }
            set
            {
                _typeOfPackage = value;

            }
        }
        public  int NoOfPeople

        {
            get
            {
                return _noOfPeople;
            }
            set
            {
                _noOfPeople = value;

            }
        }
        public  int Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        public  string CustomerName { get => _customerName; set => _customerName = value; }
        public  int CustomerContactNumber { get => _customerContactNumber; set => _customerContactNumber = value; }
        public int CustemerBookingId { get => _custemerBookingId; set => _custemerBookingId = value; }

        public Party()
            {
            CustomerName="default name";

            _customerContactNumber = 00000000;
             }

        public void InputPersonalDetails()
        {
            bool verify;
            Console.WriteLine("Please enter your name");
            CustomerName = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine("please enter your phone number");
            Console.WriteLine("");
            verify = int.TryParse(Console.ReadLine(), out _customerContactNumber);
            if (verify)
            {
                Console.WriteLine("your personal information is saved");
                Console.WriteLine("");
            }
            else
            {

                Console.WriteLine("please enter correct details");
                Console.WriteLine("");
                InputPersonalDetails();
            }
        }
        public void InputFunctionDetails()
        {

            Console.WriteLine("enter venue");
            Console.WriteLine("");
            Venue = Console.ReadLine();
            Console.WriteLine("enter date(dd/mm/yyyy)");
            Console.WriteLine("");
            Date = Console.ReadLine();
            Console.WriteLine("enter party start time(24 hrs format)");
            Console.WriteLine("");
            StartTime = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter party stop time(24 hrs format)");
            Console.WriteLine("");
            StopTime = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("please enter number of people");
            Console.WriteLine("");
            _noOfPeople = Convert.ToInt32(Console.ReadLine());
        }


        public void Packages()
        {
            bool verify;
            Console.WriteLine("");
            Console.WriteLine("we provide folowing packages... CHOOSE ONE[1/2/3]");
            Console.WriteLine("(1)SILVER STANDARD");
            Console.WriteLine("(2)GOLD STANDARD");
            Console.WriteLine("(3)PLATINUM STANDARD");
            Console.WriteLine("");
            verify = int.TryParse(Console.ReadLine(), out _typeOfPackage);
            try
            {
                {
                    switch (TypeOfPackage)
                    {
                        case 1:
                            {
                                Console.WriteLine("");
                                Console.WriteLine(" in this package 1500 per plate ");
                                Console.WriteLine("");
                                Amount = 1500;
                                Finalize();
                                break;
                            }
                        case 2:
                            {
                                Console.WriteLine("");

                                Console.WriteLine(" in this package 2000 per plate ");
                                Console.WriteLine("");
                                Amount = 2000;
                                Finalize();
                                break;
                            }
                        case 3:
                            {
                                Console.WriteLine("");
                                Console.WriteLine(" in this package 2500 per plate ");
                                Console.WriteLine("");
                                Amount = 2500;
                                Finalize();

                                break;
                            }
                        default:
                            {
                                Console.WriteLine("");
                                Console.WriteLine("please choose correct input");
                                Console.WriteLine("");
                                Packages();
                                break;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("please enter correct details"+e);
                Console.WriteLine("");
                Packages();
            }
           
        }
        public void ShowDetails()
        {
            string answer;
            Console.WriteLine("");
            Console.WriteLine("Please Check Your details");
            Console.WriteLine("");
            Console.WriteLine("          NAME-" + CustomerName);
            Console.WriteLine("");
            Console.WriteLine("          MOBILE NUMBER-" + _customerContactNumber);
            Console.WriteLine("");
            Console.WriteLine("          VENUE-" + Venue);
            Console.WriteLine("");
            Console.WriteLine("          Date-" + Date);
            Console.WriteLine("");
            Console.WriteLine("          START TIME OF EVENT- " + StartTime);
            Console.WriteLine("");
            Console.WriteLine("          END TIME OF EVENT- " + StopTime);
            Console.WriteLine("");
            Console.WriteLine("          Do you want finalize your details");
            Console.WriteLine("enter yes or no");
            Console.WriteLine("");
            answer = Console.ReadLine();
            if (answer == "yes" || answer == "YES")
            {
                Console.WriteLine(""); Console.WriteLine("");
                Console.WriteLine(" we are generating Your Bill");
               

            }
            else if (answer == "no" || answer == "NO")
            {
                Console.WriteLine("");
                Console.WriteLine("Please refill the form ");
                InputPersonalDetails();
                InputFunctionDetails();
                ShowDetails();

            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("please enter correct answer");
                ShowDetails();
            }

        }
        public virtual void Billing()
        {

        }
        public void Finalize()
        { string answer;

            Console.WriteLine("Do you Want to finalize this package ");
            Console.WriteLine("press yes or no ");
           
          answer=  Console.ReadLine();
            if (answer == "no" || answer == "NO")
            {
                Packages();
            }
            else if (answer == "yes" || answer == "YES")
            {
                Console.WriteLine(" Check your details");
            }
            else {
                Console.WriteLine("please enter correct input");
                Finalize();
            }

        }
        

    }
    
    
    
    
}
