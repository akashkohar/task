﻿using System;

namespace Automobile
{/// <summary>
/// there is an abstract class vehicle which contains abstract methods 
/// and there implimentations are in the respective classes car and bike
/// </summary>
    public abstract class Vehicle
    {
        private string _vehicleName;
        private static string _vehicleColour;
        public readonly int maxSpeed;

        public int initialSpeed;

        public static string VehicleColour
        {
            get
            {

                return _vehicleColour;

            }
            set
            {
                _vehicleColour = value;
            }

        }

        public Vehicle(string defaultName)
        {
            _vehicleName = defaultName;
            initialSpeed = 0;

        }
        private static int noOfWheels;
        public static int NoOfwheels
        {
            get
            {
                return noOfWheels;
            }

            set
            {
                noOfWheels = value;
            }
        }

        //Abstract methods of Vehicle Class
        public abstract void Start();
        public abstract void Stop();
        public abstract void SpeedUp(int addSpeed, int maxSpeed);
        public abstract void Functioning();
        public abstract void CalculateToll();
    }
    public class Bike : Vehicle
    {
        private string _BikeName;
        private static string _BikeColour;
        public readonly int maxSpeed;

        public int initialSpeed;


        public Bike(string defaultName) : base(defaultName)
        {
            _BikeName = defaultName;
            maxSpeed = 60;
            NoOfwheels = 2;

        }

        public override void Functioning()            //overriden method 
        {


            int start = 1;
            char ans;
            string localName;


            Console.WriteLine("Enter the Vehicle Colour ");
            Vehicle.VehicleColour = Console.ReadLine();




        }
        public override void SpeedUp(int addSpeed, int maxSpeed)          //overriden method 
        {
            if (initialSpeed >= 60)
            {
                Console.WriteLine("you are going over max speed");

            }
            else
            {
                initialSpeed += addSpeed;
            }

        }

        public override void Start()            //overriden method 
        {

            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + NoOfwheels);
            Console.WriteLine("Vehicle colour= " + VehicleColour);
            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your speed is=" + initialSpeed);



        }
        public override void Stop()            //overriden method 

        {

            Console.WriteLine("vehicle is stopped");
        }

        public override void CalculateToll()             //overriden method 
        {
          
            Console.WriteLine("toll cost for car is rs " );
           

        }
    }

    public class Car : Vehicle
    {
        private string _carName;
        private static string _carcolour;
        public readonly int maxSpeed;

        public int initialSpeed;

        public Car(string defaultName) : base(defaultName)
        {
            _carName = defaultName;
            maxSpeed = 60;
            NoOfwheels = 4;

        }
        public override void Functioning()        //overriden method 
        {


            int start = 1;
            char ans;
            string localName;


            Console.WriteLine("Enter the Vehicle Colour ");
            Vehicle.VehicleColour = Console.ReadLine();
          
        }

        public override void SpeedUp(int addSpeed, int maxSpeed)      //overriden method 
        {
            if (initialSpeed >= 60)
            {
                Console.WriteLine("you are going over max speed");

            }
            else
            {
                initialSpeed += addSpeed;
            }

        }
        public override void Start()                 //overriden method 
        {

            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + NoOfwheels);
            Console.WriteLine("Vehicle colour= " + VehicleColour);
            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your speed is=" + initialSpeed);
            
        }
        public override void Stop()               //overriden method 

        {

            Console.WriteLine("vehicle is stopped");
        }

        public override void CalculateToll()
        {
               Console.WriteLine("toll cost for car is rs " );
          
            

        }
    } 
    //this consist main method 
    class Control
    {
        public static void Main(string[] args)
        {
            int key, start = 1;
            string localName;
            Char ans;
            Console.WriteLine("Enter the name vehicle type (1)Bike  (2)Car");
            key = Convert.ToInt32(Console.ReadLine());

            if (key == 1)
            {
                Console.WriteLine("Enter the Vehicle Name ");
                localName = Console.ReadLine();
                Bike input = new Bike(localName);

                input.Functioning();
                input.CalculateToll();
                do
                {

                    if (start == 1)
                    {
                        input.Start();
                        Console.WriteLine("do you want to speed up(y/n)");
                        ans = Convert.ToChar(Console.ReadLine());
                        if (ans == 'y' || ans == 'Y')
                        {

                            input.SpeedUp(30, input.maxSpeed);
                        }
                    }
                    else
                        input.Stop();
                    Console.WriteLine("Do you want to (1)keep going or (2)Stop your ride(1/2)");
                    start = Convert.ToInt32(Console.ReadLine());


                } while (start == 1);

                input.Stop();
            }
            if (key == 2)
            {
                Console.WriteLine("Enter the Vehicle Name ");
                localName = Console.ReadLine();
                Car input = new Car(localName);
                input.Functioning();
                input.CalculateToll();
                do
                {

                    if (start == 1)
                    {
                        input.Start();
                        Console.WriteLine("do you want to speed up(y/n)");
                        ans = Convert.ToChar(Console.ReadLine());
                        if (ans == 'y' || ans == 'Y')
                        {

                            input.SpeedUp(30, input.maxSpeed);
                        }
                    }
                    else
                        input.Stop();
                    Console.WriteLine("Do you want to (1)keep going or (2)Stop your ride(1/2)");
                    start = Convert.ToInt32(Console.ReadLine());


                } while (start == 1);

                input.Stop();
            }

        }

    }

}
