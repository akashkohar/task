﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using FirstCoreApp.Models;
using DataSets = FirstCoreApp.Models.DataSets;

namespace FirstCoreApp.Controllers
{
    public class DataSetController : Controller
    {
        string ConnectionString = @"data source = LAPR032; integrated security = false; initial catalog = CoreMVC; user id = sa; password = Akash@123";
        // GET: DataSet
        public ActionResult Index()
        {
            DataTable dtbProduct = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {

                sqlCon.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter("select * from details;", sqlCon);
                sqlAdapter.Fill(dtbProduct);

            }
            return View(dtbProduct);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new DataSets());
        }

        // POST: DataSet/Create
        [HttpPost]
        
        public ActionResult Create(DataSets Data)
        {
            
                using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
                {
                    sqlCon.Open();
                    string query = "insert into details values(@Id,@userName,@state)";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                
                sqlCmd.Parameters.AddWithValue("@Id", Data.Id);
                   sqlCmd.Parameters.AddWithValue("@userName", Data.UserName);
                   sqlCmd.Parameters.AddWithValue("@state", Data.AddState);
                
                    sqlCmd.ExecuteNonQuery();
                }
                return RedirectToAction("index");

            }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            return View(new DataSets());
        }


        // POST: DataSet/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id,DataSets Data)
        {
           
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                string query = "update details set username=@userName,Addstate=@state where id='"+id+"'";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@userName", Data.UserName);
            
               
                sqlCmd.Parameters.AddWithValue("@state", Data.AddState);
                sqlCmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
        }

        // GET: DataSet/Delete/5
        public ActionResult Delete(int id)
        {
            using (SqlConnection sqlCon = new SqlConnection(ConnectionString))
            {
                sqlCon.Open();
                string query = "delete from details where id=@id";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@id", id);

                sqlCmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index");
           
        }

        
       
    }
}