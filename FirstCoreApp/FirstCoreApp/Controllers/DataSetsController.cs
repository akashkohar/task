﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FirstCoreApp.Models;

namespace FirstCoreApp.Controllers
{
    public class DataSetsController : Controller
    {
        private readonly DataContext _context;

        public DataSetsController(DataContext context)
        {
            _context = context;
        }

        // GET: DataSets
        public async Task<IActionResult> Index()
        {
            return View(await _context.UserTable.ToListAsync());
        }

        // GET: DataSets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataSets = await _context.UserTable
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dataSets == null)
            {
                return NotFound();
            }

            return View(dataSets);
        }

        // GET: DataSets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DataSets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,AddState")] DataSets dataSets)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dataSets);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dataSets);
        }

        // GET: DataSets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataSets = await _context.UserTable.FindAsync(id);
            if (dataSets == null)
            {
                return NotFound();
            }
            return View(dataSets);
        }

        // POST: DataSets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserName,AddState")] DataSets dataSets)
        {
            if (id != dataSets.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dataSets);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DataSetsExists(dataSets.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dataSets);
        }

        // GET: DataSets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataSets = await _context.UserTable
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dataSets == null)
            {
                return NotFound();
            }

            return View(dataSets);
        }

        // POST: DataSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dataSets = await _context.UserTable.FindAsync(id);
            _context.UserTable.Remove(dataSets);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DataSetsExists(int id)
        {
            return _context.UserTable.Any(e => e.Id == id);
        }
    }
}
