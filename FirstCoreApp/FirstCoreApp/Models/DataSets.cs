﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstCoreApp.Models
{
    public class DataSets
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [DisplayName("State")]
       
        public string AddState { get; set; }
     
    }
}
