﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CandidatesRegistraion.Models
{
    public class ScheduledInterviews
    {
        public int Id { get; set; }
        public int CandidateId { get; set; }
        public string StratDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string InterviewerName { get; set; }
        public CandidatesData Candidates { get; set; }
    }
}
