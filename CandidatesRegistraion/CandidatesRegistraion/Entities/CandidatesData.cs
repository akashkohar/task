﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CandidatesRegistraion.Models
{
    public class CandidatesData
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public long phone { get; set; }
        public string experience { get; set; }
        public string dateOfBirth { get; set; }
       
      
        public ICollection<ScheduledInterviews> InterviewData { get; set; }
    }
}
