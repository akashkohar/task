﻿
using Microsoft.EntityFrameworkCore;
using System;

namespace CandidatesRegistraion.Models
{
    public class DataContext : DbContext
    {
        private string _connectionString;
        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }


        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }
        public virtual DbSet<CandidatesData> CandidatesDatas { get; set; }

        public virtual DbSet<ScheduledInterviews> InterviewSchedule { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (!builder.IsConfigured)
            {
                builder.UseSqlServer(_connectionString);

            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CandidatesData>(data =>
            {
                data.ToTable("CandidatesDatas");

                data.HasKey(prime => prime.Id);


            });

            modelBuilder.Entity<ScheduledInterviews>(data => data.HasKey(prime => prime.Id));

            modelBuilder.Entity<ScheduledInterviews>()   //here declared CandidateID as a ForeignKey 
                                                       // for eatablishing one to many realtion
                .HasOne<CandidatesData>(dept => dept.Candidates)
                .WithMany(relation => relation.InterviewData)
                .HasForeignKey(key => key.CandidateId);

        }



        

    }


}

