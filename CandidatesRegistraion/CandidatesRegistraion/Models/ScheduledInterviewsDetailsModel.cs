﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CandidatesRegistraion.Models
{
    public class ScheduledInterviewsDetailsModel
    {
        public int Id { get; set; }
        public int CandidateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public long Phone { get; set; }
        public string Experience { get; set; }
        public string DateOfBirth { get; set; }
       
        public string StratDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string InterviewerName { get; set; }

    }
}
