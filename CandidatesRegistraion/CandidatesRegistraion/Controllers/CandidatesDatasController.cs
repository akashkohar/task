﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CandidatesRegistraion.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace CandidatesRegistraion.Controllers
{

    [Route("api/[controller]/[action]")]

    [ApiController]
    [AllowAnonymous]
    [EnableCors("Allow")]
    
    public class CandidatesDatasController : ControllerBase
    {
        private readonly DataContext _context;

        public CandidatesDatasController(DataContext context)
        {
            _context = context;
        }

        // GET: api/CandidatesDatas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CandidatesData>>> GetCandidatesDatas()
        {
            return await _context.CandidatesDatas.ToListAsync();
        }

        // GET: api/CandidatesDatas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CandidatesData>> GetCandidatesData(int id)
        {
            var candidatesData = await _context.CandidatesDatas.FindAsync(id);

            if (candidatesData == null)
            {
                return NotFound();
            }

            return candidatesData;
        }

        // PUT: api/CandidatesDatas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCandidatesData(int id, CandidatesData candidatesData)
        {
            if (id != candidatesData.Id)
            {
                return BadRequest();
            }

            _context.Entry(candidatesData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CandidatesDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CandidatesDatas
        [HttpPost]
        public async Task<ActionResult<CandidatesData>> PostCandidatesData(CandidatesData candidatesData)
        {
            _context.CandidatesDatas.Add(candidatesData);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCandidatesData", new { id = candidatesData.Id }, candidatesData);
        }

        // DELETE: api/CandidatesDatas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CandidatesData>> DeleteCandidatesData(int id)
        {
            var candidatesData = await _context.CandidatesDatas.FindAsync(id);
            if (candidatesData == null)
            {
                return NotFound();
            }

            _context.CandidatesDatas.Remove(candidatesData);
            await _context.SaveChangesAsync();

            return candidatesData;
        }

        private bool CandidatesDataExists(int id)
        {
            return _context.CandidatesDatas.Any(e => e.Id == id);
        }
    }
}
