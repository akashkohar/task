﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CandidatesRegistraion.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace CandidatesRegistraion.Controllers
{
    [Route("api/[controller]/[action]")]
    [AllowAnonymous]
    [EnableCors("Allow")]
    [ApiController]
    public class SceduledInterviewsDetailsController : ControllerBase
    {
        private readonly DataContext _context;

        public SceduledInterviewsDetailsController(DataContext context)
        {
            _context = context;
        }
        // GET: api/SceduledInterviewsDetails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ScheduledInterviewsDetailsModel>>> GetScheduledInterviewsDetails()
        {
            
            var allDetails =  _context.InterviewSchedule.Select(x => new ScheduledInterviewsDetailsModel
            {
                Id =x.Id,
                CandidateId=x.Candidates.Id,
                FirstName = x.Candidates.firstName,
                LastName = x.Candidates.lastName,
                Email = x.Candidates.email,
                Phone = x.Candidates.phone,
                Experience = x.Candidates.experience,
                DateOfBirth = x.Candidates.dateOfBirth,
                StratDateTime=x.StratDateTime,
                EndDateTime=x.EndDateTime,
                InterviewerName=x.InterviewerName
            }



            ).ToList();
            return allDetails;
        }

       

        // POST: api/SceduledInterviewsDetails
        

        // PUT: api/SceduledInterviewsDetails/5
        [HttpPut]
        public async Task<IActionResult> PutScheduledInterviewDetails(int interviewId  , ScheduledInterviewsDetailsModel data)
        {

            if ( interviewId!=data.Id)
            {
                return BadRequest();
            }

            CandidatesData candidate = new CandidatesData();
            ScheduledInterviews interviewData = new ScheduledInterviews();
            candidate.Id = data.CandidateId;
            candidate.firstName = data.FirstName;
            candidate.lastName = data.LastName;
            candidate.email = data.Email;
            candidate.phone = data.Phone;
            candidate.experience = data.Experience;

            _context.Update(candidate);
            await _context.SaveChangesAsync();

            interviewData.Id = data.Id;
            interviewData.CandidateId = data.CandidateId;
            interviewData.StratDateTime = data.StratDateTime;
            interviewData.EndDateTime = data.EndDateTime;
            interviewData.InterviewerName = data.InterviewerName;
            _context.Update(interviewData);
            await _context.SaveChangesAsync();

            return NoContent();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult<SceduledInterviewsDetailsController>> DeleteCandidatesData(int id)
        {
            var interviewData = await _context.InterviewSchedule.FindAsync(id);
            if (interviewData == null)
            {
                return NotFound();
            }
            var candidateId = _context.InterviewSchedule.Where(x => x.Id == id).Select(x => x.CandidateId);

            
            _context.InterviewSchedule.Remove(interviewData);
            
            await _context.SaveChangesAsync();

            return NoContent();
        }


    }
}
