﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CandidatesRegistraion.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace CandidatesRegistraion.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [AllowAnonymous]
    [EnableCors("Allow")]
    public class ScheduledInterviewsController : ControllerBase

    {
        private readonly DataContext _context;

        public ScheduledInterviewsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/ScheduledInterviews
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ScheduledInterviews>>> GetInterviewSchedule()
        {
            return await _context.InterviewSchedule.ToListAsync();
        }

        // GET: api/ScheduledInterviews/5
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ScheduledInterviews>> GetScheduledInterviews(int id)
        {
            var scheduledInterviews = await _context.InterviewSchedule.FindAsync(id);

            if (scheduledInterviews == null)
            {
                return NotFound();
            }

            return scheduledInterviews;
        }

        // PUT: api/ScheduledInterviews/5
        [HttpPut("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> PutScheduledInterviews(int id, ScheduledInterviews scheduledInterviews)
        {
            if (id != scheduledInterviews.Id)
            {
                return BadRequest();
            }

            _context.Entry(scheduledInterviews).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScheduledInterviewsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ScheduledInterviews
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<ScheduledInterviews>> PostScheduledInterviews(ScheduledInterviews scheduledInterviews)
        {
            _context.InterviewSchedule.Add(scheduledInterviews);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetScheduledInterviews", new { id = scheduledInterviews.Id }, scheduledInterviews);
        }

        // DELETE: api/ScheduledInterviews/5
        [HttpDelete("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<ScheduledInterviews>> DeleteScheduledInterviews(int id)
        {
            var scheduledInterviews = await _context.InterviewSchedule.FindAsync(id);
            if (scheduledInterviews == null)
            {
                return NotFound();
            }

            _context.InterviewSchedule.Remove(scheduledInterviews);
            await _context.SaveChangesAsync();

            return scheduledInterviews;
        }

        private bool ScheduledInterviewsExists(int id)
        {
            return _context.InterviewSchedule.Any(e => e.Id == id);
        }

      
        [HttpGet]
      
        public IActionResult GetNameFromId()
        {
            var names = _context.CandidatesDatas.Select(x => new
            {

                name = x.firstName,
                id=x.Id
            });

            return new JsonResult(names);

        }

    }
}
