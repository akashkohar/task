﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CandidatesRegistraion.Migrations
{
    public partial class second1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dateOfJoining",
                table: "CandidatesDatas");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "dateOfJoining",
                table: "CandidatesDatas",
                nullable: true);
        }
    }
}
