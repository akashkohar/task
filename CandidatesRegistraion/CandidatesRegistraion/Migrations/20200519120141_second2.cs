﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CandidatesRegistraion.Migrations
{
    public partial class second2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "interviewerName",
                table: "CandidatesDatas");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "interviewerName",
                table: "CandidatesDatas",
                nullable: true);
        }
    }
}
