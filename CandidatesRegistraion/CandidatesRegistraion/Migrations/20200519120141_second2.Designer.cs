﻿// <auto-generated />
using CandidatesRegistraion.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CandidatesRegistraion.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20200519120141_second2")]
    partial class second2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CandidatesRegistraion.Models.CandidatesData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("dateOfBirth");

                    b.Property<string>("email");

                    b.Property<string>("experience");

                    b.Property<string>("firstName");

                    b.Property<string>("lastName");

                    b.Property<long>("phone");

                    b.HasKey("Id");

                    b.ToTable("CandidatesDatas");
                });

            modelBuilder.Entity("CandidatesRegistraion.Models.ScheduledInterviews", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CandidateId");

                    b.Property<string>("EndDateTime");

                    b.Property<string>("InterviewerName");

                    b.Property<string>("StratDateTime");

                    b.HasKey("Id");

                    b.HasIndex("CandidateId");

                    b.ToTable("InterviewSchedule");
                });

            modelBuilder.Entity("CandidatesRegistraion.Models.ScheduledInterviews", b =>
                {
                    b.HasOne("CandidatesRegistraion.Models.CandidatesData", "Candidates")
                        .WithMany("InterviewData")
                        .HasForeignKey("CandidateId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
