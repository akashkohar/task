﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotnettrainingproject
{
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

    }
    class LinqExample
    {
        public void Operations()
        {
            //List<string> example =new List<string>() { "akash", "arif", "ashish", "prabhat" ,"ayush"};
            //var xz = from examples in example where examples.Contains("a") select examples;
            List<Person> ListPerson = new List<Person>()
            {   new Person() { Age=21, Name="akash"},
                new Person() { Age = 50, Name = "prbht" },
                new Person() { Age = 23, Name = "ashish" },
            new Person() { Age = 25, Name = "prateek" },
            new Person() { Age = 23, Name = "devansh" },
            new Person() { Age = 26, Name = "rishab" },
            new Person() { Age = 20, Name = "somvir" }};
            Console.WriteLine("oreder by ");
            var listOrederBy = (from examples in ListPerson where examples.Name.Contains("a") orderby examples.Age   select examples.Name    ).ToList();
           foreach (var val in listOrederBy)
            {
                Console.WriteLine(val.ToString());
            }
            Console.WriteLine("oreder by desc");
            var decending = (from examples in ListPerson where examples.Name.Contains("a") orderby examples.Age descending select examples.Name).ToList();
            foreach (var val in decending)
            {
                Console.WriteLine(val);
            }
            Console.WriteLine("lamda expression");
            var lamda= ListPerson.OrderBy(e => e.Name).ThenBy(e => e.Age);
            foreach (var val in lamda)
            {
                Console.WriteLine(val.Name);
            }
            Console.WriteLine("group by ");
            var groupBy = (from example in ListPerson
                                group example by example.Age);



            foreach (var val in groupBy)
            {

                // Here salary is the key value 
                Console.WriteLine("Group By age: {0}", val.Key);

                foreach (Person list in val)
                {
                    Console.WriteLine("Employee Name: {0}",
                                               list.Name);
                }
            }
            Console.WriteLine("select all ");
            var selectAll= from cust in ListPerson
                            select cust;

            foreach (var list in selectAll)
            {
                Console.WriteLine(list.Name);
            }


            Console.WriteLine("max  ");
            var maximum = (from list in ListPerson select list.Age).Max();
            Console.WriteLine(maximum);
            Console.WriteLine("  ");
            var sum = (from list in ListPerson select list.Age).Sum();
            Console.WriteLine(sum);

            Console.ReadKey();
        }
    }
}
