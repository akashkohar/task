﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DotNetTrainingProject
{
    
    class EnumExample
    {
        enum Months
        {
            Jan = 1, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
        }
        public void MonthDisplay()
        {
            Dictionary<Months, int> ob = new Dictionary<Months, int>();
            ob.Add(Months.Jan, 31);
            ob.Add(Months.Feb, 28);
            ob.Add(Months.Mar, 31);
            ob.Add(Months.Apr, 30);
            ob.Add(Months.May, 31);
            ob.Add(Months.Jun, 30);
            ob.Add(Months.Jul, 31);
            ob.Add(Months.Aug, 31);
            ob.Add(Months.Sep, 30);
            ob.Add(Months.Oct, 31);
            ob.Add(Months.Nov, 30);
            ob.Add(Months.Dec, 31);
            
            Console.WriteLine("enter a month name");
            string x = Console.ReadLine();
            foreach (var ele1 in ob)
            {
                if (ele1.Key.ToString() == x)
                {
                    Console.WriteLine(ele1.Value);
                }
                else { continue; }
            }
            Console.ReadKey();
        }

    }
}
