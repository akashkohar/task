﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTrainingProject
{
    class Person
    {
        private string _name;
        private int _age;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }
        public int Age
        {
            get => _age;

            set => _age = value;
        }

        public void Display()
        {
            int localLength;
            List<Person> nameAge = new List<Person>();
            Console.WriteLine("enter the number of record you want to register");
           localLength= Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <=localLength; i++)
            {
                Console.WriteLine("enter the name and age of the person{0}" + i);
                Name = Console.ReadLine();
                Age = Convert.ToInt32(Console.ReadLine());
                nameAge.Add(new Person { Name=Name, Age = Age });
            }
            Console.WriteLine("people with age greater than 30 are=");
            for (int i = 0; i < localLength; i++)
            {
                if (nameAge[i].Age > 30)
                {
                    Console.WriteLine("name="+nameAge[i].Name);
                    Console.WriteLine("Age=" + nameAge[i].Age);  
        }

            }

        }
    }
}
