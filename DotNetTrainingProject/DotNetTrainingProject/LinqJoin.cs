﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetTrainingProject
{
    class Boxing
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
    class Cricket
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
   public class LinqJoin
    {
        public  void InnerJoin()
        {

            Console.WriteLine("     inner join ");

            List<Boxing> listBoxing = new List<Boxing>()
            {
                new Boxing() { Name = "akash",Id = 100 },
                new Boxing() { Name = "kohar",Id = 200 },
                new Boxing() { Name = "amit",Id = 300 },
                new Boxing() { Name = "rittu",Id = 400 },
                new Boxing() { Name = "sakshi",Id = 500 },
                new Boxing() { Name = "amitu",Id = 600 },
                new Boxing() { Name = "ritik",Id = 700 },
                new Boxing() { Name = "raghavi",Id = 800 }
            };

            List<Cricket> listCricket = new List<Cricket>()
            {
                new Cricket() { Name = "akash",Id = 100 },
                new Cricket() { Name = "Agrawal",Id = 120 },
                new Cricket() { Name = "amit",Id = 300 },
                new Cricket() { Name = "Jain",Id = 30 },
                new Cricket() { Name = "ahmed",Id = 57 },
                new Cricket() { Name = "amitu ",Id = 600 },
                new Cricket() { Name = "sharma",Id = 77 },
                new Cricket() { Name = "raghavi",Id = 800 }
            };





            var Joined = (from persons in listBoxing
                          join people in listCricket
                          on persons.Id equals people.Id
                          select new { personName = persons.Name, peopleName = people.Name });

            //Console.WriteLine("First_name  Last_Name");

            foreach (var list in Joined)
            {
                Console.WriteLine( list.personName + "     " + list.peopleName);
            }


        }

        public  void LeftJoin()
        {
            Console.WriteLine("     left join ");

            List<Boxing> listBoxing = new List<Boxing>()
            {
                new Boxing() { Name = "akash",Id = 100 },
                new Boxing() { Name = "kohar",Id = 200 },
                new Boxing() { Name = "amit",Id = 300 },
                new Boxing() { Name = "rittu",Id = 400 },
                new Boxing() { Name = "sakshi",Id = 500 },
                new Boxing() { Name = "amitu",Id = 600 },
                new Boxing() { Name = "ritik",Id = 700 },
                new Boxing() { Name = "raghavi",Id = 800 }
            };


            List<Cricket> listCricket = new List<Cricket>()
            {
                new Cricket() { Name = "akash",Id = 100 },
                new Cricket() { Name = "Agrawal",Id = 120 },
                new Cricket() { Name = "amit",Id = 300 },
                new Cricket() { Name = "Jain",Id = 30 },
                new Cricket() { Name = "ahmed",Id = 57 },
                new Cricket() { Name = "amitu ",Id = 600 },
                new Cricket() { Name = "sharma",Id = 77 },
                new Cricket() { Name = "raghavi",Id = 800 }
            };

            var joinedList = from persons in listBoxing
                             join person in listCricket on persons.Id equals person.Id into List
                             from people in List
                             select new { people.Name, people.Id };

            foreach (var list in joinedList)
            {
                Console.WriteLine(list.Id +"      "+ list.Name);
            }

        }
        public void GroupJoin()


        {
            Console.WriteLine("     group join ");

            List<Boxing> listBoxing = new List<Boxing>()
            {
                new Boxing() { Name = "akash",Id = 100 },
                new Boxing() { Name = "kohar",Id = 200 },
                new Boxing() { Name = "amit",Id = 300 },
                new Boxing() { Name = "rittu",Id = 400 },
                new Boxing() { Name = "sakshi",Id = 500 },
                new Boxing() { Name = "amitu",Id = 600 },
                new Boxing() { Name = "ritik",Id = 700 },
                new Boxing() { Name = "raghavi",Id = 800 }
            };



            List<Cricket> listCricket = new List<Cricket>()
            {
                new Cricket() { Name = "akash",Id = 100 },
                new Cricket() { Name = "Agrawal",Id = 120 },
                new Cricket() { Name = "amit",Id = 300 },
                new Cricket() { Name = "Jain",Id = 309 },
                new Cricket() { Name = "ahmed",Id = 57 },
                new Cricket() { Name = "amitu ",Id = 600 },
                new Cricket() { Name = "sharma",Id = 77 },
                new Cricket() { Name = "raghavi",Id = 800 }
            };
            var valueList = from students in listBoxing
                            join persons in listCricket on students.Id equals persons.Id
                                into joinedList
                            select new
                            {
                                firstName = students.Name,
                                lastName = from list in joinedList
                                           orderby list.Name
                                           select list
                            };

            Console.WriteLine("linq joins ");
            foreach (var value in valueList)
            {
                Console.WriteLine(value.firstName);

                foreach (var val in value.lastName)
                {
                    Console.WriteLine("{0,-10} {1}       ", val.Name, val.Id);
                }

            }
        }

    }

}