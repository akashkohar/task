﻿using System;

namespace DotNetTrainingProject
{
    interface IPainter
    {
        void Paint();


    }
    interface ChangeSeatCover
    {
        void SeatCover();
    }


    public class Vehicle : IPainter
    {
        private string _vehicleName;
        private static string _vehicleColour;
        public readonly int maxSpeed;
        public enum NumberOfWheels
        {
            car = 0,
            bike = 2
        }
        public int initialSpeed;


        public  string VehicleColour
        {
            get
            {

                return _vehicleColour;

            }
            set
            {
                _vehicleColour = value;
            }

        }
        public Vehicle(string defaultName)
        {
            _vehicleName = defaultName;
            initialSpeed = 0;
            maxSpeed = 60;
        }
        private  int _noOfWheels;
        public  int NoOfwheels
        {
            get
            {
                return _noOfWheels;
            }

            set
            {
                _noOfWheels = value;
            }
        }






        public void Stop()

        {

            Console.WriteLine("vehicle is stopped");
        }

        public void SpeedUp(int addSpeed, int maxSpeed)
        {
            if (initialSpeed >= 60)
            {
                Console.WriteLine("you are going over max speed");

            }
            else
            {
                initialSpeed += addSpeed;

            }
        }

        public void Functioning()
        {


            int start = 1;
            char ans;
            string localName;


            Console.WriteLine("Enter the Vehicle Colour ");
           VehicleColour = Console.ReadLine();




        }

        public void Paint()
        {
            Console.WriteLine("we are painted your car  " + VehicleColour);


        }
    }
    public class Bike : Vehicle, ChangeSeatCover
    {
        private string _BikeName;
        private static string _BikeColour;
        public readonly int maxSpeed;

        public int initialSpeed;
        public void SeatCover()
        {
            Console.WriteLine("you are a valuable customer for us../n we are changing your seat cover for free ");
        }
        public void Start()
        {
            NoOfwheels =(int) NumberOfWheels.bike;
            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + NoOfwheels);
            Console.WriteLine("which colour you want for your vehicle " + VehicleColour);
            Paint();

            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your speed is=" + initialSpeed);
            SeatCover();


        }
        public Bike(string defaultName) : base(defaultName)
        {
            _BikeName = defaultName;
            maxSpeed = 60;
            NoOfwheels = 2;
        }

        public void CalculateToll()
        {

            Console.WriteLine("toll cost for bike is rs 20 ");


        }


    }


    public class Car : Vehicle, ChangeSeatCover
    {
        private string _carName;
        private static string _carColour;
        public readonly int maxSpeed;

        public int initialSpeed;
        public void SeatCover()
        {
            Console.WriteLine("you are a valuable customer for us../n we are changing your seat cover for free ");
        }
        public void Start()
        {
            NoOfwheels =(int) NumberOfWheels.car;
            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + NoOfwheels);
            Console.WriteLine("Vehicle colour= " + VehicleColour);
            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your speed is=" + initialSpeed);
            SeatCover();


        }

        public Car(string defaultName) : base(defaultName)
        {
            _carName = defaultName;
            NoOfwheels = 4;
            maxSpeed = 60;
        }


        public void CalculateToll()
        {

            Console.WriteLine("toll cost for car is rs  40");


        }
    }

    class Control
    {
        public void Localmain()
        {
            int key, start = 1;
            string localName;
            Char ans;
            Console.WriteLine("Enter the name vehicle type (1)Bike  (2)Car");
            key = Convert.ToInt32(Console.ReadLine());

            if (key == 1)
            {
                Console.WriteLine("Enter the Vehicle Name ");
                localName = Console.ReadLine();
                Bike input = new Bike(localName);

                input.Functioning();
                input.CalculateToll();
                do
                {

                    if (start == 1)
                    {
                        input.Start();
                        Console.WriteLine("do you want to speed up(y/n)");
                        ans = Convert.ToChar(Console.ReadLine());
                        if (ans == 'y' || ans == 'Y')
                        {

                            input.SpeedUp(30, input.maxSpeed);
                        }
                    }
                    else
                        input.Stop();
                    Console.WriteLine("Do you want to (1)keep going or (2)Stop your ride(1/2)");
                    start = Convert.ToInt32(Console.ReadLine());


                } while (start == 1);

                input.Stop();
            }
            if (key == 2)
            {
                Console.WriteLine("Enter the Vehicle Name ");
                localName = Console.ReadLine();
                Car input = new Car(localName);
                input.Functioning();
                input.CalculateToll();
                do
                {

                    if (start == 1)
                    {
                        input.Start();
                        Console.WriteLine("do you want to speed up(y/n)");
                        ans = Convert.ToChar(Console.ReadLine());
                        if (ans == 'y' || ans == 'Y')
                        {

                            input.SpeedUp(30, input.maxSpeed);
                        }
                    }
                    else
                        input.Stop();
                    Console.WriteLine("Do you want to (1)keep going or (2)Stop your ride(1/2)");
                    start = Convert.ToInt32(Console.ReadLine());


                } while (start == 1);

                input.Stop();
            }

            Console.ReadKey();
        }

       
    }

}
  

        

