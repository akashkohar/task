﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DotNetTrainingProject
{
    class FileHandling
    {
        public void FileHand()
        {
            string str,str2=" ";
            FileStream TxtFile = new FileStream("test.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter ob = new StreamWriter(TxtFile);
            Console.WriteLine("enter text to write in the file");
            str = Console.ReadLine();
            ob.WriteLine(str);
            TxtFile.Dispose();
            
            Console.WriteLine("your file contain =\n");
            try
            {
                StreamReader ob1 = new StreamReader(TxtFile);
                ob1.BaseStream.Seek(0, SeekOrigin.Begin);
                while (str2 != null)
                {
                    str2 = ob1.ReadLine();
                    Console.WriteLine(str2);
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("not found");
            }
            
        }
    }
}
