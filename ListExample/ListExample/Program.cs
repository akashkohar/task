﻿using System;
using System.Collections.Generic;
namespace ListExample
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 32, 4, 24, 3, 4, 3, 4 };
            List<int> list = new List<int>(arr);
            Console.WriteLine("origional array");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.WriteLine("origional list");
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
            
            list.Add(100);
            Console.WriteLine("array after list modification");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }

            Console.WriteLine("list after modification ");
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
            List<int> list1 = new List<int>();
            list1.AddRange(list);
            list1.Add(99);
            Console.WriteLine("this is list1");
            for (int i = 0; i < list1.Count; i++)
            {
                Console.WriteLine(list1[i]);
            }

        }
    }
}