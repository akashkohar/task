﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace UserAdminAuthentication.Controllers
{
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> __roleManager;
        public RoleController(RoleManager<IdentityRole> roleManager)
        {
            __roleManager = roleManager;
        }
        public IActionResult Index()
        {
            var roles = __roleManager.Roles.ToList();
            return View(roles);
        }
        public IActionResult Create()
        {
            return View(new IdentityRole());
        }
        [HttpPost]
        public async Task<IActionResult> Create(IdentityRole role)
        {
            await __roleManager.CreateAsync(role);
            return RedirectToAction("Index");


        }
    }
}