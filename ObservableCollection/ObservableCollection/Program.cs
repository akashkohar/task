﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace observationColl
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(4);
            list.Add(5);
            list.Add(2);
            list.Add(8);
            list.Add(3);
            Console.WriteLine("origional list");
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);

            }
            ObservableCollection<int> ob1 = new ObservableCollection<int>(list);
            ObservableCollection<int> ab = ob1;
            ob1.Add(100);

            Console.WriteLine("observable collection after modification");
            for (int i = 0; i < ab.Count; i++)
            {
                Console.WriteLine(ab[i]);

            }




            Console.WriteLine("list after modification of observable   collection");
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);

            }
            Console.ReadKey();
        }
    }
}