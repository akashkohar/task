<!DOCTYPE HTML>

<html>

<head>
<title>Registeration Form</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="using.css"/>

<script src="javascript.js"></script>

</head>


<body>
<div class ="container" style="background-color:black"><h1></h1></div>


<div class="container">
<form class="form-horizontal" id="myForm" onSubmit="return nameFunction()" method="post" style="width: 500px; margin:auto">
<h1>Add Employee</h1><br>

<div class="form-group row">
<label for "fname" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Name</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="text" class="form-control" id="fname">
<p id="nameCheck"></p>

</div>
</div>


<div class="form-group row">
<label for "birthday" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Date Of Birth</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="date" class="form-control" id="birthday">
<p id="birthdayCheck"></p>

</div>
</div>


<div class="form-group row">
<label for "phone" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Phone</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="text" class="form-control" id="phone" placeholder="(222)222 22222">
<p id="phoneCheck"></p>

</div>
</div>


<div class="form-group row">
<label for "email" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Email</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="text" class="form-control" id="email">
<p id="emailCheck"></p>
</div>
</div>

<div class="form-group row">
<label for "department" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Department</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">

<select class="form-control >
<option value="xamarin">Select</option>
        <option value="xamarin">Xamarin</option>
        <option value="dotnet">DotNet</option>
        <option value="node">Node</option>
        <option value="php">PHP</option>
        <option value="business">Business Analyst</option>
      </select>
</div>
</div>


 <div class="form-group row">
    <label for="address" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Address</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
    <textarea class="form-control" id="address" rows="3"></textarea>
	<p id="addCheck"></p>
  </div>
  </div>


<div class="form-group row">
<label for "state" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">State</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">

<!-- <select class="form-control> --> 
<!-- <option value="select">Select</option> -->
        <!-- <option value="up">Uttar Pradesh</option> -->
        <!-- <option value="delhi">Delhi</option> -->
        <!-- <option value="mp">Madhya Pradesh</option> -->
        <!-- <option value="haryana">Haryana</option> -->
      <!-- </select> -->
<!-- </div> -->
<!-- </div> -->


<!-- <div class="form-group row"> -->
<!-- <label for "city" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">City</label> -->
<!-- <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12"> -->

<!-- <select onclick="selected()" class="form-control"> -->
<!-- <option value="select">Select</option> -->
    <!-- <option value="newdelhi">New Delhi</option> -->
    <!-- <option value="noida">Noida</option> -->
    <!-- <option value="gurugram">Gurugram</option> -->
<!-- <option value="lucknow">Lucknow</option> -->
 <!-- </select> -->
 <!-- </div> -->
 <!-- </div> -->

<select id="SelectState" size="1" onchange="SubData(this.value)" class="form-control" >
 <option value="none" selected disabled hidden>             </option>
 <option>Delhi</option>
<option>UP</option>
<option>Haryana</option>
</select><br>
  </div>
  </div>
     <div class="row">
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
   <label for="selcity">City</label>
</div>
 <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
 <select id="SelCity"  class="form-control" >
 <option value="none" selected disabled hidden>             </option>
   <option></option>
 </select><br>
 </div>
 </div>


<div class="form-group row">
<label for "zip" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Zip</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="text" class="form-control " name="zip" placeholder="(22)22222" id="Zip">
<p id="ZipCheck"></p>
</div>
</div>



<div class="form-group row">
<label for "joining" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Joining Date</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="date" class="form-control " name="joining" id="JoiningDate">
<p id="JoiningDates"></p>
</div>
</div>


<div class="form-group row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
<button onclick = "Validate()" type="submit" style = "margin-left: 70% " class ="btn btn-primary" class="form-control" >submit</button>
<button type="close-quote" class="btn" >cancel</button>
</div>
</div>
</form>


</body>
</html>