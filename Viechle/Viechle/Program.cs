﻿using System;

namespace Automobile
{
    class Vehicle
    {
        private string _vehicleName;
        private static string _vehicleColour;
        public const int maxSpeed = 60;

        public int initialSpeed;


        public static string VehicleColour
        {
            get
            {

                return _vehicleColour;

            }
            set
            {
                _vehicleColour = value;
            }

        }
        public Vehicle(string defaultName)
        {
            _vehicleName = defaultName;
            initialSpeed = 0;
        }
        private static int noOfWheels;
        public static int NoOfwheels
        {
            get
            {
                return noOfWheels;
            }

            set
            {
                noOfWheels = value;
            }
        }


        void Start()
        {

            Console.WriteLine("The Vehicle is start running ");

            Console.WriteLine("Vehicle number of wheels " + Vehicle.NoOfwheels);
            Console.WriteLine("Vehicle colour= " + VehicleColour);
            Console.WriteLine("Your MAX SPEED = " + maxSpeed);
            Console.WriteLine("your current speed  =" + initialSpeed);



        }



        void Stop()

        {

            Console.WriteLine("vehicle is stopped");
        }

        void SpeedUp(int addSpeed)
        {
            if (initialSpeed >= 60)
            {
                Console.WriteLine("you cannot go over max speed");


            }
            else
            {

                initialSpeed += addSpeed;
            }

        }

        static void Main(string[] args)
        {


            int start = 1;
            char ans;
            string localName;



            Console.WriteLine("Enter the Vehicle Name ");
            localName = Console.ReadLine();


            Vehicle input = new Vehicle(localName);


            Console.WriteLine("Enter the Vehicle Colour ");
            Vehicle.VehicleColour = Console.ReadLine();
            Console.WriteLine("Enter the No. of wheels ");
            Vehicle.NoOfwheels = Convert.ToInt32(Console.ReadLine());
            do
            {

                if (start == 1)
                {
                    input.Start();
                    Console.WriteLine("do you want to speed up(y/n)");
                    ans = Convert.ToChar(Console.ReadLine());
                    if (ans == 'y' || ans == 'Y')
                    {

                        input.SpeedUp(30);
                    }
                }
                else
                    input.Stop();
                Console.WriteLine("Do you want to (1)keep going  or (2)Stop your ride(1/2)");
                start = Convert.ToInt32(Console.ReadLine());

            } while (start == 1);

            input.Stop();



        }


    }
}