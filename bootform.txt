<doctype! html>
<html><head><title>Trainee reg</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css"></link>
</head>
<body>

<div class ="container" style="background-color:black"><h1></h1></div>

<form action="" style="width: 500px; margin:auto">
<div class="page-header">

<h1>Add Employee</h1>
</div>

<div class=" form-group row" >
<label for ="txt_Name" class="col-lg-6 col-md-6 col-sm-9 col-xs-12" >Name</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="txt" name="txt_Name" id="txt_Name" class="form-control">
</div>
</div>


<div class=" form-group row" >
<label for ="date_DOB" class="col-lg-6 col-md-6 col-sm-9 col-xs-12" >DOB</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="datetime" name="date_DOB" id="date_DOB" class="form-control ">
</div>
</div>

<div class=" form-group row" >
<label for ="num_Phone" class="col-lg-6 col-md-6 col-sm-9 col-xs-12" >Phone</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="number" name="num_Phone" id="num_Phone" class="form-control ">
</div>
</div>

<div class="form-group row">
<label for="email_Email" class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Email</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="email" class="form-control" id="email_Email" name="email_Email">
</div>
</div>
<div class="form-group row">
<label for="Department"  class="col-lg-6 col-md-6 col-sm-9 col-xs-12">Department</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<select name="Department" id="Department" class="form-control">
<option value="react">React</option>
<option value="node">Node</option>
<option value=".net">.NET</option>
<option value="xamerin">Xamerin</option>
<option value ="select one " selected disabled>select-one</option>
</select>
</div>
</div>


<div class="form-group row">
<label for ="txtArea_Adress" class="col-lg-6 col-md-6 col-sm-9 col-xs-12" >Address</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<textarea type="textarea" id="txtArea_Adress" name="txtArea_Adress" class="form-control"></textarea>
</div>
</div>

<div class="form-group row">
<label for="State"  class="col-lg-6 col-md-6 col-sm-9 col-xs-12">state</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<select name="State" id="State" class="form-control">
<option value="Delhi">Delhi</option>
<option value="UP">UP</option>
<option value="Punjab">Punjab</option>
<option value="Haryana">Haryana</option>
<option value ="select one " selected disabled>select-one</option>
</select>
</div>
</div>

<div class="form-group row">
<label for="City"  class="col-lg-6 col-md-6 col-sm-9 col-xs-12">City</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<select name="City" id="City" class="form-control">
<option value="New Delhi">New Delhi</option>
<option value="Noida>UP</option>
<option value="Amritsar">Amritsar</option>
<option value="Gurgaon">Gurgaon</option>
<option value ="select one " selected disabled>select-one</option>
</select>
</div>
</div>

<div class="form-group row">
<label for ="num_Zip" class="col-lg-6 col-md-6 col-sm-9 col-xs-12" >ZIP</label>
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<input type="number" id="num_Zip" name="num_Zip" class="form-control">
</div>
</div>


<div class="form-group row">
<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
<button type="submit" onclick="alert('submittion successfully')" class ="btn btn-primary" class="form-control" >submit</button>
<button type="close-quote" class="btn" >cancel</button>
</div>
</div>
</form>
</body>
</html>